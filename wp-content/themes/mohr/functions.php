<?php
/**
 * Templateheld functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Templateheld
 */

if ( ! function_exists( 'templateheld_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function templateheld_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Templateheld, use a find and replace
	 * to change 'templateheld' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'templateheld', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'templateheld' ),
		'menu-2' => esc_html__( 'Footer', 'templateheld' ),
		'menu-3' => esc_html__( 'Footer Plus', 'templateheld' ),
		'menu-4' => esc_html__( 'Socialmedia', 'templateheld' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'templateheld_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 250,
		'width'       => 250,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'templateheld_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function templateheld_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'templateheld_content_width', 640 );
}
add_action( 'after_setup_theme', 'templateheld_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function templateheld_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'templateheld' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'templateheld' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'templateheld_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function templateheld_scripts() {
	wp_enqueue_style( 'templateheld-production-style', get_stylesheet_uri() );
	// TODO: Include minified style.min.css
	// wp_enqueue_style( 'templateheld-style', get_template_directory_uri() . '/style.min.css' );

	wp_enqueue_script( 'templateheld-navigation', get_template_directory_uri() . '/js/min/navigation.min.js', array(), '20151215', true );

	wp_enqueue_script( 'templateheld-skip-link-focus-fix', get_template_directory_uri() . '/js/min/skip-link-focus-fix.min.js', array(), '20151215', true );

	// Add jQuery and Popper.js for Bootstrap 4
	wp_enqueue_script( 'templateheld-jquery', get_template_directory_uri() . '/js/min/jquery.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-jquery-ui', get_template_directory_uri() . '/js/min/jquery-ui.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-popper', get_template_directory_uri() . '/js/min/popper.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-bootstrap', get_template_directory_uri() . '/js/min/bootstrap.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-js', get_template_directory_uri() . '/js/min/main.min.js', array(), '', true );

	// Add owl carousel2 JS
	wp_enqueue_script( 'templateheld-carousel', get_template_directory_uri() . '/js/min/owl.carousel.min.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'templateheld_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Allow custom file mimes in media upload
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
	$mimes['vcf'] = 'text/x-vcard';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Change a currency symbol
 */
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'EUR': $currency_symbol = 'EUR'; break;
     }
     return $currency_symbol;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
	switch( $currency ) {
		case 'EUR_EUR': $currency_symbol = 'EUR'; break;
	}
	return $currency_symbol;
}

add_image_size( 'teaser_shop', 576);
add_image_size( 'team_member', 9999, 480);

// Replace special chars and umlauts
function clean($string, $strict = false) {
	if ($strict) {
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Remove all special chars.
	} else {
		$umlauts = [
			['Ä', 'ae'],
			['ä', 'ae'],
			['Ö', 'oe'],
			['ö', 'oe'],
			['Ü', 'ue'],
			['ü', 'ue'],
			['ß', 'ss'],
		];

		// Go through umlauts array
		for ($i=0; $i < count($umlauts); $i++) {
			$string = str_replace($umlauts[$i][0], $umlauts[$i][1], $string);
		}

		$string = preg_replace('/\./', '', $string); // Remove all dots.
		$string = preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Replaces all special chars with spaces.
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	}

	return $string;
}

// Replace <strong> with <span class="color-primary">
function strongToColor($string) {
	$string = str_replace('<strong>', '<span class="color-primary">', $string);
	$string = str_replace('</strong>', '</span>', $string);

	return $string;
}

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce');
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

// Remove Woocommerce stuff
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

add_filter( 'woocommerce_get_image_size_thumbnail', function( $size ) {
  return array(
	  'width'  => 320,
	  'height' => 260,
	  'crop'   => 1,
  );
});


// function paypal_checkout_icon() {
//     // pls return the new logo/image URL here
//     return 'http://www.url.to/your/new/logo.png';
// }
// add_filter( 'woocommerce_paypal_icon', 'paypal_checkout_icon' );


// Change payment title heading by Woocommerce Germanized

add_action( 'woocommerce_review_order_before_payment', 'woocommerce_eonm_template_checkout_payment_title' );
if ( ! function_exists( 'woocommerce_eonm_template_checkout_payment_title' ) ) {

	/**
	 * Checkout payment gateway title
	 */
	function woocommerce_eonm_template_checkout_payment_title() {
		if (WC()->cart->needs_payment()) {
			echo '<h2 id="order_payment_heading">' . __( 'Choose a Payment Gateway', 'woocommerce-germanized' ) . '</h2>';
		}
	}

}
// Outcommented title function in /Users/saengduan/Work/Work Work/mohr-wp/public/wp-content/plugins/woocommerce-germanized/includes/wc-gzd-template-hooks.php (Row 144)
#remove_action( 'woocommerce_review_order_before_payment', 'woocommerce_gzd_template_checkout_payment_title' );

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields', 99 );

function custom_override_checkout_fields( $fields ) {

    unset($fields['billing']['billing_postcode']['validate']);
    unset($fields['shipping']['shipping_postcode']['validate']);
		$fields['order']['order_comments']['required'] = true;

    return $fields;
}


/**
 * @snippet       WooCommerce Remove Product Permalink @ Order Table
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=20455
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 2.6.8
 */

add_filter( 'woocommerce_order_item_name', 'remove_permalink_order_table', 10, 3 );

function remove_permalink_order_table( $name, $item, $order ) {
   $name = $item['name'];
   return $name;
}


pll_register_string('Shop Danke', 'Thank you.', 'Shop');
pll_register_string('Shop Bestellung erfolgreich', 'Your order has been received.', 'Shop');
pll_register_string('Shop Entschuldigung', 'Unfortunately your order cannot be processed.', 'Shop');
pll_register_string('Shop Bestellung fehlgeschlagen', 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'Shop');
pll_register_string('Zurück zur Übersicht', 'Back to overview', 'WordPress');
pll_register_string('Mehr', 'Read more', 'WordPress');
pll_register_string('Rechtsgebiete Detaillink', 'More', 'WordPress');
pll_register_string('Rechtsanwälte – Vita', 'Vita', 'WordPress');
pll_register_string('Rechtsanwälte – Telefon', 'Telephone', 'WordPress');
pll_register_string('Rechtsanwälte – Mitgliedschaften', 'Memberships', 'WordPress');
pll_register_string('Rechtsanwälte – Erfahrung', 'Experience', 'WordPress');
pll_register_string('Rechtsanwälte – Vorträge', 'Talks', 'WordPress');
pll_register_string('Rechtsanwälte – Publikationen', 'Publications', 'WordPress');
pll_register_string('Rechtsanwälte – mehr erfahren', 'Learn more', 'WordPress');
pll_register_string('Helper – Landscape Fix', 'You have changed the viewport. The page will be reloaded now. Please be patient.', 'WordPress');
pll_register_string('Shop – Produkt-Linktext', 'Learn more', 'Shop');
pll_register_string('Shop – Hinzufügen', 'Add to cart', 'Shop');
pll_register_string('Shop – Slogan', 'Individual advice on a fixed price', 'Shop');
pll_register_string('Shop – Zusätzliche Informationen', 'Additional information', 'Shop' );
pll_register_string('Shop – Zusätzliche Informationen Subtext', 'Additional information subtext', 'Shop' );
pll_register_string('Shop – Zusätzliche Informationen Platzhalter', 'Additional information placeholder', 'Shop' );
pll_register_string('Shop – Anrede Platzhalter', 'Select your title', 'Shop' );
pll_register_string('Shop – Anrede Mr. Dr.', 'Mr. Dr.', 'Shop' );
pll_register_string('Shop – Anrede Mr. Dr.', 'Ms. Dr.', 'Shop' );
pll_register_string('Shop – Anwalt Lawyer', 'Lawyer', 'Shop' );
pll_register_string('E-Mail – Zahlung erhalten Part 1', 'Hi there. Thank you! We have successfully received your payment for order', 'Shop' );
pll_register_string('E-Mail – Zahlung erhalten Part 2', '. Your order is now being processed.', 'Shop' );
pll_register_string('Shop – Zahlungmöglichkeiten Zusatz', 'Bezahlvorgänge für Kreditkarte, Lastschrift, Kauf auf Rechnung, benötigen kein Paypal-Konto.', 'Shop' );


//Add New Pay Button Text
add_filter( 'woocommerce_product_single_add_to_cart_text', 'themeprefix_cart_button_text' );
add_filter( 'woocommerce_product_add_to_cart_text', 'themeprefix_cart_button_text' );

function themeprefix_cart_button_text() {
 return pll__( 'Add to cart');
}


add_filter( 'woocommerce_checkout_fields', 'webendev_woocommerce_checkout_fields' );
/**
 * Change Order Notes Placeholder Text - WooCommerce
 *
 */
function webendev_woocommerce_checkout_fields( $fields ) {
	$fields['billing']['billing_title']['options'] = array_reverse($fields['billing']['billing_title']['options']);
	$fields['billing']['billing_title']['options'][] = pll__('Select your title');
	$fields['billing']['billing_title']['options'] = array_reverse($fields['billing']['billing_title']['options']);
	$fields['order']['order_comments']['label'] = pll__( 'Additional information' );
	$fields['order']['order_comments']['placeholder'] = pll__('Additional information placeholder');
	return $fields;
}

add_filter( 'woocommerce_gzd_title_options', 'mohr_add_title_options', 10, 1 );
function mohr_add_title_options( $options ) {
    $options[3] = pll__('Mr. Dr.');
    $options[4] = pll__('Ms. Dr.');
    return $options;
}

/**
 * @snippet       WooCommerce Only one product in cart
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=560
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.4.3
 */

add_filter( 'woocommerce_add_to_cart_validation', 'bbloomer_only_one_in_cart', 99, 2 );

function bbloomer_only_one_in_cart( $passed, $added_product_id ) {

// empty cart first: new item will replace previous
wc_empty_cart();

// display a message if you like
// wc_add_notice( 'New product added to cart!', 'notice' );

return $passed;
}

// add_filter('woocommerce_admin_order_date_format', 'cw_custom_post_date_column_time');
// function cw_custom_post_date_column_time($h_time, $post)
// {
//     return get_the_time(__('Y/m/d G:i', 'woocommerce'), $post);
// }

<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header();




/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
#do_action( 'woocommerce_before_main_content' );

?>

<section id="shop">
  <div class="row header-shop">
    <header class="col-12 content">
      <div class="row no-gutters m-0">
        <div class="col-12 col-sm-auto">
          <h2 class="h1">Online<br><span><?php echo pll__( 'Lawyer') ?></span></h2>
        </div>
        <div class="col-12 col-sm shop-slogan">
          <h3 class="h2"><?php echo pll__( 'Individual advice on a fixed price') ?></h3>
        </div>
      </div>
    </header>
  </div>

  <?php
    $sections = get_field('inhaltsblocke_before', wc_get_page_id('shop'));

    foreach ($sections as $section) :
      $section_name = $section['acf_fc_layout'];
      $section_name = clean($section_name);
      $section_attr = 'id="' . $section_name . '"';

      if (strpos($section_name, '-block')) {
        $section_attr = 'class="' . $section_name . '"';
      }
  ?>


      <?php if (locate_template('template-parts/' . 'section-' . $section_name . '.php')): ?>

        <?php include(locate_template('template-parts/' . 'section-' . $section_name . '.php')); ?>

      <?php else: ?>

        <div class="row">

          <div class="col-12 col-sm-10 content">
            <?php echo $section['inhalt'] ?>
          </div>

        </div>

      <?php endif; ?>

  <?php endforeach; ?>

    <?php
    $sections = get_field('inhaltsblocke', wc_get_page_id('shop'));

    foreach ($sections as $section) :
        $section_name = $section['acf_fc_layout'];
        $section_name = clean($section_name);
        $section_attr = 'id="' . $section_name . '"';

        if (strpos($section_name, '-block')) {
            $section_attr = 'class="' . $section_name . '"';
        }
        ?>


        <?php if (locate_template('template-parts/' . 'section-' . $section_name . '.php')): ?>

        <?php include(locate_template('template-parts/' . 'section-' . $section_name . '.php')); ?>

    <?php else: ?>

        <div class="row">

            <div class="content">
                <?php echo $section['inhalt'] ?>
            </div>

        </div>

    <?php endif; ?>

    <?php endforeach; ?>

  <div class="row">
    <div class="col-12 products">
			<div class="row">

				<?php
				if ( woocommerce_product_loop() ) {

					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked wc_print_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );

					woocommerce_product_loop_start();

					if ( wc_get_loop_prop( 'total' ) ) {
						while ( have_posts() ) {
							the_post();

							/**
							 * Hook: woocommerce_shop_loop.
							 *
							 * @hooked WC_Structured_Data::generate_product_data() - 10
							 */
							do_action( 'woocommerce_shop_loop' );

							wc_get_template_part( 'content', 'product' );
						}
					}

					woocommerce_product_loop_end();

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}

				?>

				</div>
			</div>
		</div>
	</div>
</section>

<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
#do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
#do_action( 'woocommerce_sidebar' );

get_footer();

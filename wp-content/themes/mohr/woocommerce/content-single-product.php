<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<section id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>

	<div class="row header-shop">
    <header class="col-12 content">
      <div class="row no-gutters m-0">
        <div class="col-12 col-sm-auto">
          <h2 class="h1">Online<br><span>Lawyer</span></h2>
        </div>
        <div class="col-12 col-sm shop-slogan">
          <h3 class="h2"><?php echo pll__( 'Individual advice on a fixed price') ?></h2>
        </div>
      </div>
    </header>
  </div>

	<div class="row">
		<div class="col content">
			<?php the_content(); ?>
		</div>
		<?php if (has_post_thumbnail()): ?>
			<div class="col-12 col-sm-auto image-wrapper">
				<?php the_post_thumbnail('full'); ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
    $sections = get_field('inhaltsblocke', wc_get_page_id('shop'));

    foreach ($sections as $section) :
      $section_name = $section['acf_fc_layout'];
      $section_name = clean($section_name);
      $section_attr = 'id="' . $section_name . '"';

      if (strpos($section_name, '-block')) {
        $section_attr = 'class="' . $section_name . '"';
      }
  ?>


      <?php if (locate_template('template-parts/' . 'section-' . $section_name . '.php')): ?>

        <?php include(locate_template('template-parts/' . 'section-' . $section_name . '.php')); ?>

      <?php else: ?>

        <div class="row">

          <div class="content">
            <?php echo $section['inhalt'] ?>
          </div>

        </div>

      <?php endif; ?>

  <?php endforeach; ?>

		<div class="summary entry-summary">
			<?php
				/**
				 * Hook: woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				do_action( 'woocommerce_single_product_summary' );
			?>
		</div>
	</div>

	<?php do_action( 'woocommerce_after_single_product' ); ?>

</section>

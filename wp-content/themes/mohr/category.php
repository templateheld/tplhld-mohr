<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

get_header(); ?>

<?php
	$term = get_queried_object();
	$section_id = $term->slug;
	$title = get_field('kategorie_titel', $term);
	$image = get_field('kategorie_bild', $term);
	$image_src = wp_get_attachment_image_src( $image, 'full' )[0];
	$image_srcset = wp_get_attachment_image_srcset( $image, 'full' );
	$image_sizes = wp_get_attachment_image_sizes( $image, 'full' );
	$image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
	$description = get_field('kategorie_beschreibung', $term);
	$overlay_color = get_field('overlay_farbe', $term);
	$show_post_link = get_field('zeige_mehr_link', $term);
?>

<section id="<?php echo $section_id; ?>" class="content-category">
	<div class="row">
		<header class="col-12 image-wrapper">
      <img src="<?php echo esc_attr( $image_src );?>"
      srcset="<?php echo esc_attr( $image_srcset ); ?>"
      sizes="<?php echo esc_attr( $image_sizes );?>"
      alt="<?php echo esc_attr( $image_alt );?>">
      <div class="overlay<?php echo ($overlay_color == 'Gelb' ? ' overlay-yellow' : ''); ?>">
        <h1><?php echo $title['titel_top'] ?><br><span><?php echo $title['titel_bottom'] ?></span></h1>
      </div>
    </header>
		<div class="col-12 category-description">
      <?php echo $description; ?>
    </div>
	</div>
  <div class="row">
		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article class="col-12 article">
					<div class="row">
						<div class="col-12 col-sm-6 article-content-wrapper">
							<h2 class="date"><?php echo get_the_date('d.n.Y', get_the_ID()); ?></h2>
							<h2><?php the_title(); ?></h2>
							<div class="article-content">
								<?php the_excerpt(); ?>
								<?php if ($show_post_link): ?>
									<a href="<?php the_permalink(); ?>"><?php echo pll__('Read more'); ?></a>
								<?php endif; ?>
							</div>
						</div>

						<?php if (has_post_thumbnail()): ?>
							<div class="col-12 col-sm-6 image-wrapper">
					      <?php the_post_thumbnail('full'); ?>
								<div class="overlay-empty<?php echo ($overlay_color == 'Gelb' ? ' overlay-yellow' : ''); ?>"></div>
					    </div>
				    <?php endif; ?>
					</div>
				</article>

			<?php endwhile; ?>

			<div class="col-12 category-navigation">
				<?php
					$prev_text = 'Mehr ' . $title['titel_top'] . $title['titel_bottom'];

					the_posts_navigation([
						'next_text' => 'Zurück',
						'prev_text' => $prev_text
					]);
				?>
			</div>

		<?php else : ?>


		<?php endif; ?>
	</div>
</section>

<?php
get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>
<?php
	$is_multi_lingual = false;

	$frontpage_id = get_option( 'page_on_front' );
	$frontpage_german_id = pll_get_post($frontpage_id, 'de');
	$frontpage_german = get_post($frontpage_german_id);

	if ($frontpage_german->post_status == 'publish') {
		$is_multi_lingual = true;
	}

	$term = get_queried_object();
	$is_eyecatcher_dark = get_field('ist_kategorie_bild_dunkel', $term);
	if ($is_eyecatcher_dark[0]) {
		$header_class = ' class="visible"';
	}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header id="header"<?php echo $header_class; ?>>
		<div class="row">
			<div id="logo" class="col">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php echo get_template_directory_uri() . '/assets/img/logo.svg' ?>" alt="<?php echo get_bloginfo('title'); ?>">
				</a>
			</div>
			<nav id="menu-primary-container" class="col-auto col-xl-2">
				<button class="menu-primary-trigger" data-toggle="open"></button>
				<div id="menu-primary-wrapper">
					<?php if (function_exists('pll_the_languages') && $is_multi_lingual): ?>
						<div id="language-switch">
							<ul><?php pll_the_languages();?></ul>
						</div>
					<?php endif; ?>
					<button class="menu-primary-trigger" data-toggle="close"></button>
					<div id="menu-primary-body">
						<?php
							$main_nav = array(
								'menu' => 'primary',
								'menu_class' => ' ',
								'container' => ''
							);

							wp_nav_menu($main_nav);
						?>
					</div>
				</div>
			</nav>
		</div>
	</header>

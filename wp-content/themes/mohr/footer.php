<?php
/**
* The template for displaying the footer
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package Templateheld
*/

?>

<?php
  $id = $post->ID;
  if (is_shop()) {
    $id = wc_get_page_id('shop');
  }

  if(is_category()) {
    $id = get_queried_object();
  }

  $teasers = get_field('teaser', $id);


  if ($teasers) {
    include(locate_template('template-parts/teaser.php'));
  }
?>

<footer id="footer" class="small-gutters">
  <div class="row">
    <div class="col-12 content">
      <nav id="menu-footer-container">
        <?php
          $nav = array(
            'menu' => 'footer',
            'menu_class' => ' ',
            'container' => ''
          );

          wp_nav_menu($nav);
        ?>
      </nav>

      <nav id="menu-footer-plus-container">
        <?php
          $nav = array(
            'menu' => 'footer-plus',
            'menu_class' => ' ',
            'container' => ''
          );

          wp_nav_menu($nav);
        ?>
      </nav>

      <nav id="menu-socialmedia-container">
        <?php
          $nav = array(
            'menu' => 'socialmedia',
            'menu_class' => ' ',
            'container' => ''
          );

          wp_nav_menu($nav);
        ?>
      </nav>

      <p class="copyright">&copy; Copyright Medizinkanzlei Mohr, <?php echo date('Y'); ?></p>
    </div>
  </div>
</footer>
<section id="modal-reload">
    <div class="container d-flex h-100">
        <div class="row justify-content-center align-self-center">
            <div class="col">
                <p><?php echo pll__('You have changed the viewport. The page will be reloaded now. Please be patient.'); ?></p>
            </div>
        </div>
    </div>
</section>
<?php wp_footer(); ?>

</body>
</html>

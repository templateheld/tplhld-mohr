// Classes
function Helper() {
  this.breakpoints = {
    xs: 500,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200
  },
  this.viewportFactors = {
    default: 4,
    xs: 2.5,
    sm: 1
  },
  this.staticFactor = 1800 / 100,   // At this viewport (1800px) should all elements be based on this vw.
  this.checkBreakpoint = function(breakpoint) {
    var breakpoints = this.breakpoints;

    for (var key in breakpoints) {
      if (breakpoints.hasOwnProperty(key)) {
        if (breakpoint == key) {
          breakpoint = breakpoints[key];
        }
      }
    }

    if (jQuery(window).width() >= breakpoint) {
      return true;
    } else {
      return false;
    }
  },
  this.getVwValue = function(value) {
    var breakpoints = this.breakpoints,
        viewportFactors = this.viewportFactors,
        vwFactor;

    vwFactor = this.viewportFactors.default;

    if (this.checkBreakpoint('xs')) {
      vwFactor = this.viewportFactors.xs;
    }

    if (this.checkBreakpoint('sm')) {
      vwFactor = this.viewportFactors.sm;
    }

    value = value / this.staticFactor;

    value = value * (jQuery(window).width() / 100) * vwFactor;

    return value;
  },
  this.isInView = function(elem, offset) {
    var isInView = false,
        offset = offset;

    // Check if offset is a number
    if (isNaN(offset)) {
      offset = 0;
    }

    offset = this.getVwValue(offset);

    if (jQuery(elem).length) {
      if (parseFloat(jQuery(elem).offset().top) + parseFloat(offset) <= jQuery(window).scrollTop() + jQuery(window).height() && parseFloat(jQuery(elem).offset().top) + parseFloat(offset) + parseFloat(jQuery(elem).outerHeight()) > jQuery(window).scrollTop()) {
        isInView = true;
      }
    }

    return isInView;
  }
}

function NavToggle() {
  this.elems = {
    wrapper: '#menu-primary-wrapper',
    trigger: {
      elem: '.menu-primary-trigger',
      toggle: 'data-toggle'
    },
    body: 'body'
  },
  this.states = {
    active: 'nav-active'
  },
  this.activate = function() {
    jQuery(this.elems.body).addClass(this.states.active);
  },
  this.deactivate = function() {
    jQuery(this.elems.body).removeClass(this.states.active);
  },
  this.init = function() {
    var navToggle = this,
        trigger,
        wrapper,
        toggle;

    jQuery(document).on('click', function(event) {
      trigger = jQuery(event.target).closest(navToggle.elems.trigger.elem);
      wrapper = jQuery(event.target).closest(navToggle.elems.wrapper);
      toggle = jQuery(trigger).attr(navToggle.elems.trigger.toggle);

      if (trigger.length && toggle == 'open') {
        navToggle.activate();
      }

      if (!trigger.length && !wrapper.length || toggle == 'close'){
        navToggle.deactivate();
      }
    });
  }
}

// Used on 'Team page' for bottom-to-top-moving animation
function HoverCollapse() {
  this.helper = new Helper(),
  this.elems = {
    collapse: '[data-toggle="hover-collapse"]'
  },
  this.getHeight = function(elem) {
    var height;

    jQuery(elem).attr('style', 'display: block !important');
    height = jQuery(elem).outerHeight();
    jQuery(elem).removeAttr('style');

    return height;
  },
  this.setHeight = function(target, height) {
    jQuery(target).css({
      transform: 'translateY(' + height + 'px)'
    });
  },
  this.reset = function() {
    jQuery(this.elems.collapse).each(function() {
      parent = jQuery(this).attr('data-parent');
      parent = jQuery(this).parents(parent);
      target = jQuery(this).attr('data-target');
      target = jQuery(this).parents(target);

      jQuery(parent, target).removeAttr('style');
      jQuery(this).hide();
    });
  }
  this.prepareOverlay = function() {
    var hoverCollapse = this,
        parent,
        target,
        height;

    jQuery(this.elems.collapse).each(function() {
      parent = jQuery(this).attr('data-parent');
      parent = jQuery(this).parents(parent);
      target = jQuery(this).attr('data-target');
      target = jQuery(this).parents(target);

      jQuery(parent).css('overflow', 'hidden');

      height = hoverCollapse.getHeight(this);
      hoverCollapse.setHeight(target, height);

      jQuery(this).attr('style', 'display: block !important');
    });
  },
  this.initOverlay = function() {
    var target;

    jQuery(this.elems.collapse).each(function() {
      target = jQuery(this).attr('data-target');
      target = jQuery(this).parents(target);

      jQuery(target).addClass('hover-collapse');
    });
  },
  this.init = function() {
    var hoverCollapse = this;

    if (this.helper.checkBreakpoint('sm')) {
      this.prepareOverlay();

      setTimeout(function() {
        hoverCollapse.initOverlay();
      }, 100);
    }

    jQuery(window).on('resize', function() {
      if (hoverCollapse.helper.checkBreakpoint('sm')) {
        hoverCollapse.prepareOverlay();
      } else {
        hoverCollapse.reset();
      }
    })
  }
}

function MiniNav() {
  this.helper = new Helper(),
  this.elems = {
    body: 'body'
  },
  this.states = {
    active: 'mini-nav',
    ready: 'mini-nav-ready'
  },
  this.options = {
    offset: 200
  },
  this.prepare = function() {
    jQuery(this.elems.body).addClass(this.states.ready);
  },
  this.unprepare = function() {
    jQuery(this.elems.body).removeClass(this.states.ready)
  },
  this.minify = function() {
    jQuery(this.elems.body).addClass(this.states.active);
  },
  this.reset = function() {
    jQuery(this.elems.body).removeClass(this.states.active);
  },
  this.initMiniNav = function() {
    var offset = this.options.offset;

    offset = this.helper.getVwValue(offset);

    /*if (hoverCollapse.helper.checkBreakpoint('sm')) {
      this.prepare();
      if (jQuery(window).scrollTop() >= this.options.offset) {
        this.minify();
      } else {
        this.reset();
      }
    } else {
      this.unprepare();
      this.reset();
    }*/
    this.prepare();
    if (jQuery(window).scrollTop() >= this.options.offset) {
        this.minify();
    } else {
        this.reset();
    }

  },
  this.init = function() {
    var miniNav = this;

    miniNav.initMiniNav();

    jQuery(window).on('scroll resize', function() {
      miniNav.initMiniNav();
    })
  }
}

function AnimationTrigger() {
  this.helper = new Helper(),
  this.elems = {
    elem: '[data-animation]'
  },
  this.states = {
    active: 'active'
  },
  this.options = {
    elemOffset: 'data-offset'
  },
  this.activate = function(elem) {
    jQuery(elem).addClass(this.states.active);
  },
  this.deactivate = function(elem) {
    jQuery(elem).removeClass(this.states.active);
  },
  this.initAnimations = function() {
    var animationTrigger = this,
        offset;

    jQuery(this.elems.elem).each(function() {
      offset = jQuery(this).attr(animationTrigger.options.elemOffset);
      if (animationTrigger.helper.isInView(this, offset)) {
        animationTrigger.activate(this);
      } else {
        animationTrigger.deactivate(this);
      }
    })
  },
  this.init = function() {
    var animationTrigger = this;

    this.initAnimations();

    jQuery(window).on('scroll resize', function() {
      animationTrigger.initAnimations();
    })
  }
}

function ProductAnimationTrigger() {
  this.helper = new Helper(),
  this.elems = {
    container: '.template-shop-link',
    wrapper: '.product-animation-slide-wrapper',
    slide: '[data-product-animation]'
  },
  this.options = {
    delay: 'data-delay'
  },
  this.states = {
    active: 'step-'
  },
  this.intervals = [],
  this.resetIntervals = function() {
    for (var i = 0; i < this.intervals.length; i++) {
      clearInterval(this.intervals[i])
    }

    this.intervals = [];
  },
  this.activate = function() {
    var productAnimationTrigger = this,
        i = 1,
        intervalID;

    intervalID = setInterval(function () {
      productAnimationTrigger.reset();
      jQuery(productAnimationTrigger.elems.wrapper).addClass('step-' + i)
      i++;

      if (i == jQuery(productAnimationTrigger.elems.slide).length + 1) {
        i = 1;
      }
    }, 3000);

    this.intervals.push(intervalID);
  },
  this.reset = function() {
    jQuery(this.elems.wrapper).removeClass(function (index, className) {
      return (className.match (/(^|\s)step-\S+/g) || []).join(' ');
    });
  },
  this.isShopTeaserInView = function() {
    var isShopTeaserInView;

    if (animationTrigger.helper.isInView(this.elems.container)) {
      isShopTeaserInView = true;
    } else {
      isShopTeaserInView = false;
    }

    return isShopTeaserInView;
  },
  this.initAnimations = function() {
    if (this.intervals.length > 1) {
      this.resetIntervals();
    }

    if (this.isShopTeaserInView()) {
      this.activate();
    } else {
      this.reset();
    }

  },
  this.init = function() {
    var animationTrigger = this;

    this.initAnimations();

    jQuery(window).on('scroll resize', function() {
      animationTrigger.initAnimations();
    })
  }
}

function Carousel() {
  this.elems = {
    carousel: '.owl-carousel',
    active: '.owl-stage .active',
    all: '.owl-stage .owl-item'
  },
  this.callBack = function () {
      /*var $stage = $('.owl-stage'),
          stageW = $stage.width(),
          $el = $('.owl-item'),
          elW = 0;
      $el.each(function() {
          elW += $(this).width()+ +($(this).css("margin-right").slice(0, -2))
      });
      if ( elW > stageW ) {
          $stage.width( elW );
          console.log($stage.width( elW ));
      };*/
    /*var active = this.elems.active,
        all = this.elems.owlall;*/

    /*jQuery(this.elems.carousel).on('translate.owl.carousel', function(e){
      console.log(e.item.index);
        jQuery(active).css({
            left: -20,
            right: 0
        });
    });*/

  },
  this.init = function() {
    jQuery(this.elems.carousel).owlCarousel({
      smartSpeed: 850,
      loop:true,
      center:true,
      responsiveClass:true,
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:true,
      stagePadding: 0,
      margin: 0,
      dots: true,
      nav: false,
      //onInitialized: this.callBack(),
      //onRefreshed: this.callBack(),
      responsive: {
        0: {
          items: 1,
          autoWidth: true
        },
        576 : {
          autoWidth: true,
          //stagePadding: 400,
          margin: 100,
          dots: false
        }
     }
    });
  }
}

function MixBlendMode() {
  this.elems = {
    body: 'body',
    overlay: '.overlay',
    support: '<div class="overlay-multiply"></div>'
  },
  this.states = {
    support: 'mix-blend-mode'
  },
  this.isSupported = function() {
    var support = false;

    if (typeof window.getComputedStyle(document.body).mixBlendMode !== 'undefined') {
      support = true;
    }

    return support;
  },
  this.addSupport = function() {
    var mixBlendMode = this;

    if (this.isSupported()) {
      jQuery(this.elems.body).addClass(this.states.support);
      // jQuery(this.elems.overlay).each(function() {
      //   jQuery(mixBlendMode.elems.support).prependTo(this);
      // });
    }
  },
  this.init = function() {
    this.addSupport();
  }
}

function SmoothScroll() {
  this.elems = {
    body: 'html, body',
    link: 'a[data-scroll]'
  },
  this.miniNav = new MiniNav(),
  this.getTargetOffset = function(elem) {
    var target = jQuery(elem).attr('href');
        offset = 0;

    // Get target offset
    offset = offset + jQuery(target).offset().top;

    return offset;
  },
  this.scrollTo = function() {
    var smoothScroll = this,
        target,
        offset;

    jQuery(this.elems.link).on('click', function(e) {
      e.preventDefault();
      target = jQuery(this).attr('href');
      offset = smoothScroll.getTargetOffset(this);

      jQuery(smoothScroll.elems.body).animate({
        scrollTop: offset
      }, 850, 'easeOutExpo', function() {
        window.location.hash = target;
      });
    })
  },
  this.init = function() {
    this.scrollTo();
  }
}

function CheckoutFix() {
  this.elems = {
    billing_title: '#billing_title option:first-child',
    place_order: '#place_order',
    errorMsg: '.woocommerce-error'
  },
  this.states = {
    placeholder: {
      disabled: 'disabled',
      selected: 'selected'
    }
  },
  this.emptyVal = function(elem) {
    jQuery(elem).attr('value', '');
  },
  this.makePlaceholder = function(elem) {
    jQuery(elem).attr(this.states.placeholder.disabled, '');
  },
  this.hideError = function() {
    var checkout = this;

    jQuery(this.elems.place_order).on('click', function() {
      setTimeout(function() {
        jQuery(checkout.elems.errorMsg).fadeOut(850);
      }, 7000)
    });
  },
  this.init = function() {
    this.emptyVal(this.elems.billing_title);
    this.makePlaceholder(this.elems.billing_title);
    this.hideError();
  }
}

/*
* mix blend mode quick fix - a dirty dirty dirty fix! don't like this...but not time
* need html markup improvement for a better fix
* aydin / 17.10.18
 */
function FixBlendMode() {
  this.elems = {
    overlay: '.overlay'
  },
  this.cloneDivs = function() {
    //clone the before element and set background to none and opacity to 0
    jQuery(this.elems.overlay).each(function( index ) {
        jQuery( this ).clone().addClass('background-off').insertAfter(jQuery( this ));
    });
  },
  this.init = function() {
     this.cloneDivs();
  }
}

function MobileLandscapeFix() {
  this.elems = {
    modal: '#modal-reload'
  },
  this.mobileCheck = function () {
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) && jQuery(window).width() <= 980 ) {
          return true;
      } else {
          return false;
      }
  },
  this.mobileLandscapeFix= function () {
    var modal = jQuery(this.elems.modal),
        mobileCheck = this.mobileCheck(),
        orientationCheck = window.matchMedia("(orientation: landscape)").matches;

      jQuery(window).on('resize', function(e) {
        if(orientationCheck === false && window.matchMedia("(orientation: landscape)").matches) {
          jQuery('body').css('overflow','hidden');
          modal.show();
          setTimeout(function(){
            location.reload();
          },100);
        } else if( orientationCheck === true && window.matchMedia("(orientation: portrait)").matches ) {
          jQuery('body').css('overflow','hidden');
          modal.show();
          setTimeout(function(){
            location.reload();
          },100);
        }
    });
  },
  this.init = function() {
      this.mobileLandscapeFix();
  }
}

function MobileOwlFix() {
  this.elems = {
    overlay: '.overlay.background-off .owl-carousel',
    fixedelm: '.overlay.background-off .owl-carousel',
    item: '.owl-carousel .owl-item .item'
  },
  this.mobileCheck = function () {
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) && jQuery(window).width() <= 576 ) {
          return true;
      } else {
          return false;
      }
  },
  this.mobileFix = function () {
    if(this.mobileCheck()) {
      var heightStr = jQuery(this.elems.overlay).css('height')
          getHeight = heightStr.replace('px',''),
          factor = Number(getHeight)*1,
          height = '-'+factor+'px';

        jQuery(this.elems.overlay).css({
              top: '-80vh',
              position: 'absolute',
              left: 0,
              right: 0
            });
        //jQuery(this.elems.item).css('width','300px');
    }
  },
  this.init = function() {
      this.mobileFix();
  }
}

// Objects
var navToggle = new NavToggle();
var hoverCollapse = new HoverCollapse();
var miniNav = new MiniNav();
var animationTrigger = new AnimationTrigger();
var carousel = new Carousel();
var mixBlendMode = new MixBlendMode();
var smoothScroll = new SmoothScroll();
var checkoutFix = new CheckoutFix();
var fixBlendMode = new FixBlendMode();
var MobileOwlFix = new MobileOwlFix();
var MobileLandscapeFix = new MobileLandscapeFix()
// var productAnimationTrigger = new ProductAnimationTrigger();

// DOM
jQuery(document).ready(function() {
  navToggle.init();
  hoverCollapse.init();
  miniNav.init();
  animationTrigger.init();
  fixBlendMode.init();
  carousel.init();
  mixBlendMode.init();
  smoothScroll.init();
  checkoutFix.init();
  MobileOwlFix.init();
  MobileLandscapeFix.init();
    // productAnimationTrigger.init();
  setTimeout(function(){
    MobileOwlFix.init();
  }, 2000);
})

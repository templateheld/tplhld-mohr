<?php
/**
 * Template part for displaying page content in page-home.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  $sections = get_field('startseiten_sektionen', $post->ID);

  if ($sections) {
    foreach ($sections as $section) :
      $section_name = $section['acf_fc_layout'];
      $section_name = clean($section_name);
      $section_attr = 'id="' . $section_name . '"';

      if (strpos($section_name, '-block')) {
        $section_attr = 'class="' . $section_name . '"';
      }

      if ($section_name == 'awards') {
        $section_attr .= ' class="small-gutters"';
      }
?>

<section <?php echo $section_attr ?>>

  <?php if (locate_template('template-parts/' . 'section-' . $section_name . '.php')): ?>

    <?php include(locate_template('template-parts/' . 'section-' . $section_name . '.php')); ?>

  <?php else: ?>

    <div class="row">

      <div class="col-12 col-sm-10 content fade move-in" data-animation data-offset="100">
        <?php echo $section['inhalt'] ?>
      </div>

    </div>

  <?php endif; ?>

</section>

<?php
    endforeach;
  }
?>

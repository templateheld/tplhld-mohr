<?php
/**
 * Template part for displaying teaser based on "Good Talk" template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  $good_talk_image = $teaser['hintergrundbild'];
  $good_talk_image_src = wp_get_attachment_image_src( $good_talk_image, 'full' )[0];
  $good_talk_image_srcset = wp_get_attachment_image_srcset( $good_talk_image, 'full' );
  $good_talk_image_sizes = wp_get_attachment_image_sizes( $good_talk_image, 'full' );
  $good_talk_image_alt = get_post_meta( $good_talk_image, '_wp_attachment_image_alt', true);
  $good_talk_title = str_replace(["\r\n", "\r", "\n"], '<strong>', $teaser['uberschrift']) . '</strong>';
  $good_talk_cat = $teaser['kategorie'];
  $good_talk_posts = [
    'posts_per_page'    => -1,
    'category'          => $good_talk_cat
  ];

  $good_talk_posts = get_posts($good_talk_posts);
  $teaser_link = $teaser['teaser_link'];
  $teaser_color = $teaser['overlay_farbe'];
  $teaser_color_class = '';

  if ($teaser_color == 'Gelb') {
    $teaser_color_class = ' overlay-yellow';
  }
  if ($teaser_color == 'Leicht Orange') {
    $teaser_color_class = ' overlay-light-orange';
  }


?>
<div class="row">
  <div class="col-12 image-wrapper">
    <img src="<?php echo esc_attr( $good_talk_image_src );?>"
    srcset="<?php echo esc_attr( $good_talk_image_srcset ); ?>"
    sizes="<?php echo esc_attr( $good_talk_image_sizes );?>"
    alt="<?php echo esc_attr( $good_talk_image_alt );?>">
    <header class="overlay<?php echo $teaser_color_class; ?>">
      <h2 class="h1"><?php echo $good_talk_title ?></h2>
    </header>
  </div>
  <div class="col-12 content overlay<?php echo $teaser_color_class; ?>">

      <div class="owl-carousel">

        <?php
          $i = 0;
          foreach ($good_talk_posts as $good_talk_post):
            $good_talk_post_content = $good_talk_post->post_content;
            $good_talk_post_ID = $good_talk_post->ID;
            $i++;
        ?>
          <div class="item" style="width: 64rem">
            <?php echo apply_filters('the_content', $good_talk_post_content); ?>
          </div>
        <?php endforeach; ?>

      </div>

  </div>
</div>
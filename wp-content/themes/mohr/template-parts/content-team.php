<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php $section_id = strtolower(clean(get_the_title())); ?>

<section id="<?php echo $section_id; ?>" class="content-team small-gutters">
  <div class="row">
    <div class="col-12 col-sm-7 content">
      <?php the_content(); ?>
    </div>


    <?php
      $lawyers_highlighted = array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'post_parent'    => $post->ID,
        'order'          => 'ASC',
        'orderby'        => 'menu_order',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-lawyer-highlighted.php'
      );

      $lawyers_highlighted = new WP_Query($lawyers_highlighted);
      $i = 0;

      if ($lawyers_highlighted->have_posts()) :
    ?>

      <div class="col-12 lawyers-highlighted">

        <?php while ( $lawyers_highlighted->have_posts() ) : $lawyers_highlighted->the_post(); ?>

          <?php
            $template = get_page_template_slug();
            $name = get_field('name');
            $lawyer_id = strtolower(clean($name));
            $position = get_field('position');
            $image = get_field('teaser_bild');
            $text = get_field('teaser_text_erweitert');
            if (!$image) {
              $image = get_post_thumbnail_id();
            }
            $image_src = wp_get_attachment_image_src( $image, 'full' )[0];
            $image_srcset = wp_get_attachment_image_srcset( $image, 'full' );
            $image_sizes = wp_get_attachment_image_sizes( $image, 'full' );
            $image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
            $image_width = wp_get_attachment_image_src( $image, 'full' )[1];
            $i++;
            if ($i > 1) {
              $animation = ' data-animation data-offset="150"';
              $animation_classes = ' fade move-in';
            }
            $lawyer_offset = '';
            if($i % 2) {
                $lawyer_offset = '1';
            } else {
                $lawyer_offset = '2';
            }
          ?>

            <div class="row">
        			<div id="lawyer-<?php echo $lawyer_id; ?>" class="col-12 col-sm-auto lawyer lawyer-<?php echo $lawyer_offset; ?><?php echo $animation_classes; ?>"<?php echo $animation; ?>>
                  <img src="<?php echo esc_attr( $image_src );?>"
                  srcset="<?php echo esc_attr( $image_srcset ); ?>"
                  sizes="<?php echo esc_attr( $image_sizes );?>"
                  alt="<?php echo esc_attr( $image_alt );?>">
                  <a class="col-12 overlay" href="<?php the_permalink(); ?>">
                    <div class="row">
                      <div class="col-12 col-sm-9">
                        <h2><?php echo $name; ?></h2>
                        <h3 class="p"><?php echo $position; ?></h3>
                      </div>
                    </div>
                    <span><?php echo pll__('Read more') ?></span>
                    <!-- <div data-toggle="hover-collapse" data-parent=".lawyer" data-target=".overlay" class="overlay-content">
                      <?php echo $text; ?>
                    </div> -->
                  </a>
        	    </div>
            </div>

        <?php endwhile; ?>

      </div>

    <?php endif; wp_reset_postdata(); ?>

    <?php
      $lawyers = array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'post_parent'    => $post->ID,
        'order'          => 'ASC',
        'orderby'        => 'menu_order',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-lawyer.php'
      );

      $lawyers = new WP_Query($lawyers);
      $j = $i;
      if ($lawyers->have_posts()) :
    ?>

      <div class="col-12 lawyers-highlighted lawyers-last">

        <?php while ( $lawyers->have_posts() ) : $lawyers->the_post(); ?>

          <?php
            $template = get_page_template_slug();
            $name = get_field('name');
            $position = get_field('position');
            $image = get_field('teaser_bild');
            if (!$image) {
              $image = get_post_thumbnail_id();
            }
            $image_src = wp_get_attachment_image_src( $image, 'full' )[0];
            $image_srcset = wp_get_attachment_image_srcset( $image, 'full' );
            $image_sizes = wp_get_attachment_image_sizes( $image, 'full' );
            $image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
            $image_width = wp_get_attachment_image_src( $image, 'full' )[1];
            $lawyers_highlighted_offset = '';
            if(!($j % 2)) {
                $lawyers_highlighted_offset = '1';
            } else {
                $lawyers_highlighted_offset = '2';
            }
            $j++;
          ?>

            <div class="row">
        			<div class="col-12 col-sm-auto lawyer lawyer-<?php echo $lawyers_highlighted_offset; ?> fade move-in" data-animation data-offset="150">
                <?php if ($image_width > 350): ?>
                  <?php
                    #$image_src = wp_get_attachment_image_src( $image, 'full' )[0];
                    $image_srcset = wp_get_attachment_image_srcset( $image, 'full' );
                    $image_sizes = wp_get_attachment_image_sizes( $image, 'full' );
                   ?>
                  <div class="image-equalizer">
                    <img src="<?php echo esc_attr( $image_src );?>"
                    srcset="<?php echo esc_attr( $image_srcset ); ?>"
                    sizes="<?php echo esc_attr( $image_sizes );?>"
                    alt="<?php echo esc_attr( $image_alt );?>">
                  </div>
                <?php else: ?>
                  <img src="<?php echo esc_attr( $image_src );?>"
                  srcset="<?php echo esc_attr( $image_srcset ); ?>"
                  sizes="<?php echo esc_attr( $image_sizes );?>"
                  alt="<?php echo esc_attr( $image_alt );?>">
                <?php endif; ?>
                <div class="col-12 overlay">
                  <h2><?php echo $name; ?></h2>
                  <h3 class="p"><?php echo $position; ?></h3>
                </div>
        	    </div>
            </div>

        <?php endwhile; ?>

      </div>

    <?php endif; wp_reset_postdata(); ?>

  </div>
</section>

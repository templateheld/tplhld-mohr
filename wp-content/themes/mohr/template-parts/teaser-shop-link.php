<?php
/**
 * Template part for displaying teaser based on "Shop Link" template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php $animation_slides = $teaser['shop_teaser_animation']; ?>


<div class="row header-shop">
  <header class="col-12 content">
    <div class="row no-gutters m-0">
      <div class="col-12 col-sm-auto">
        <h2 class="h1">Online<br><span><?php echo pll__( 'Lawyer') ?></span></h2>
      </div>
      <div class="col-12 col-sm shop-slogan">
        <h3 class="h2"><?php echo pll__( 'Individual advice on a fixed price') ?></h3>
      </div>
    </div>
  </header>
  <!-- <div class="col-12 products">
    <a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>" class="row products-scroll-container fade move-in" data-animation data-offset="300">
      <?php
        $products_cat = 'mohr-produkte-de';

        if (pll_current_language() != 'de') {
          $products_cat = 'mohr-products-en';
        }

        $mohr_products = array(
          'post_type'      => 'product',
          'posts_per_page' => -1,
          'product_cat'    => $products_cat,
          'order' => 'ASC'
        );

        $mohr_products = new WP_Query( $mohr_products );

        while ( $mohr_products->have_posts() ) : $mohr_products->the_post();
          global $product;
      ?>

      <div class="col-12 product">
        <div class="row">
          <div class="col-12 col-sm-auto image-wrapper">
            <?php echo woocommerce_get_product_thumbnail('teaser_shop'); ?>
          </div>
          <div class="col-12 col-sm product-content">
            <h3><?php the_title(); ?></h3>
            <?php the_excerpt(); ?>
            <p class="woocommerce-Price-amount-wrapper"><?php echo wc_price($product->get_price()); ?></p>
          </div>
        </div>
      </div>

      <?php
        endwhile;
        wp_reset_query();
      ?>

      <div class="col-12 product-animation-container">
        <div class="row">
          <div class="col-12 product-animation-slide-wrapper">

            <?php
              foreach ($animation_slides as $animation_slide):
                $image = $animation_slide;
                $image_src = wp_get_attachment_image_src( $image, 'full' )[0];
                $image_srcset = wp_get_attachment_image_srcset( $image, 'full' );
                $image_sizes = wp_get_attachment_image_sizes( $image, 'full' );
                $image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
            ?>
              <div class="product-animation-slide" data-product-animation>
                <img src="<?php echo esc_attr( $image_src );?>"
                srcset="<?php echo esc_attr( $image_srcset ); ?>"
                sizes="<?php echo esc_attr( $image_sizes );?>"
                alt="<?php echo esc_attr( $image_alt );?>">
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </a>
  </div> -->
  <a class="col-12 content" href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>">
    <div class="row">
      <div class="col-12 col-sm align-self-center text-center">
        <span class="h1"><?php echo $teaser['shop_link_text_2']; ?></span>
      </div>
      <div class="col col-sm-auto">
        <span class="shop-link"><?php echo $teaser['shop_link_text']; ?></span>
      </div>
    </div>
  </a>
</div>

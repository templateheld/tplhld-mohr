<?php
/**
 * Template part for displaying section "Awards"
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  // Awards template
  if ($section_name == 'awards') {
    $award_title = $section['uberschrift'];
    $award_image_1 = $section['bild_1'];
    $award_image_1_src = wp_get_attachment_image_src( $award_image_1, 'full' )[0];
    $award_image_1_srcset = wp_get_attachment_image_srcset( $award_image_1, 'full' );
    $award_image_1_sizes = wp_get_attachment_image_sizes( $award_image_1, 'full' );
    $award_image_1_alt = get_post_meta( $award_image_1, '_wp_attachment_image_alt', true);
    // $award_image_2 = $section['bild_2'];
    // $award_image_2_src = wp_get_attachment_image_src( $award_image_2, 'full' )[0];
    // $award_image_2_srcset = wp_get_attachment_image_srcset( $award_image_2, 'full' );
    // $award_image_2_sizes = wp_get_attachment_image_sizes( $award_image_2, 'full' );
    // $award_image_2_alt = get_post_meta( $award_image_2, '_wp_attachment_image_alt', true);
    $award_link = $section['link'];

    // Replace <h*>-tags
    $award_title = str_replace('<h2>', '<h3 class="h2">', $award_title);
    $award_title = str_replace('</h2>', '</h3>', $award_title);
    $award_title = str_replace('<h1>', '<h2 class="h1">', $award_title);
    $award_title = str_replace('</h1>', '</h2>', $award_title);
?>
  <a class="row" href="<?php echo $award_link['url']; ?>" target="<?php echo $award_link['target']; ?>">
    <header class="col-12 col-sm-6 content">
      <?php echo $award_title; ?>
    </header>
    <div id="awards-images" class="col-12 col-sm-auto mini-gutters">
      <div class="row">
        <div class="col-10 ml-auto col-sm-auto ml-sm-0">
          <img src="<?php echo esc_attr( $award_image_1_src );?>"
          srcset="<?php echo esc_attr( $award_image_1_srcset ); ?>"
          sizes="<?php echo esc_attr( $award_image_1_sizes );?>"
          alt="<?php echo esc_attr( $award_image_1_alt );?>">
        </div>
        <!-- <div class="col-5 col-sm-auto">
          <img src="<?php echo esc_attr( $award_image_2_src );?>"
          srcset="<?php echo esc_attr( $award_image_2_srcset ); ?>"
          sizes="<?php echo esc_attr( $award_image_2_sizes );?>"
          alt="<?php echo esc_attr( $award_image_2_alt );?>">
        </div> -->
      </div>
    </div>
    <div class="col-12 col-sm-6 content">
      <span><?php echo $award_link['title']; ?>
    </div>
  </a>

<?php } ?>

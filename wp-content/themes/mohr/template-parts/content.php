<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  $section_id = strtolower(clean(get_the_title()));
  $category_id = get_the_category();
?>

<section id="<?php echo $section_id; ?>" class="content-page">
  <div class="row">
    <div class="col-12 content">
			<h1><?php the_title(); ?></h1>
      <?php the_content(); ?>
    </div>

    <?php if (has_post_thumbnail()): ?>
			<div class="col-12 col-sm-auto ml-auto image-wrapper">
	      <?php the_post_thumbnail('full'); ?>
	      <div class="overlay overlay-empty overlay-yellow"></div>
	    </div>
    <?php endif; ?>

    <div class="col-12 category-navigation posts-navigation">
  		<div class="nav-links">
        <div class="nav-next">
          <a href="<?php echo get_category_link($category_id[0]->term_id) ?>"><?php echo pll__( 'Back to overview' ); ?></a>
        </div>
      </div>
    </div>


  </div>
</section>

<?php
/**
 * Template part for displaying page content in page-rechtsgebiete.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php $section_id = strtolower(clean(get_the_title())); ?>

<section id="<?php echo $section_id; ?>" class="content-index small-gutters">
  <div class="row">
    <div class="col-12 content">
      <?php the_content(); ?>
      <div class="row">
        <div class="col-12 col-sm-5 mr-auto">
          <div class="link-collection">

            <?php
              $rechtsgebiete = array(
                  'post_type'      => 'page',
                  'posts_per_page' => -1,
                  'post_parent'    => $post->ID,
                  'order'          => 'ASC',
                  'orderby'        => 'title'
               );

              $rechtsgebiete = new WP_Query($rechtsgebiete);

              #var_dump($rechtsgebiete);

              if ( $rechtsgebiete->have_posts() ) : ?>

                  <?php while ( $rechtsgebiete->have_posts() ) : $rechtsgebiete->the_post(); ?>
                    <?php
                      $teaser_text = get_field('teaser_text');
                      $rechtsgebiete_template = get_page_template_slug($post->ID);

                    ?>

                    <?php if ($rechtsgebiete_template == 'page-about-us.php'): ?>
                      <p>
                        <strong>
                          <a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a>
                        </strong>
                      </p>

                    <?php else: ?>

                      <p>
                        <a data-toggle="collapse" href="#<?php echo $post->post_name; ?>" aria-expanded="false" aria-controls="<?php echo $post->post_name; ?>"><?php the_title(); ?></a>
                      </p>
                      <div class="collapse link-text" id="<?php echo $post->post_name; ?>">
                        <?php echo $teaser_text; ?>
                        <a href="<?php the_permalink(); ?>"><?php echo pll__('More') ?></a>
                      </div>

                    <?php endif; ?>

                  <?php endwhile; ?>

              <?php endif; wp_reset_postdata(); ?>

          </div>
        </div>
        <?php if (has_post_thumbnail()): ?>
    			<div class="col-12 col-sm-auto mr-auto image-wrapper">
    	      <?php the_post_thumbnail('full'); ?>
    	    </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>

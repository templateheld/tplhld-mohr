<?php
/**
 * Template part for displaying page content in page-contact.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  $section_id = strtolower(clean(get_the_title()));
  $address = get_field('adresse');
  $address_link = get_field('anfahrt_link');
?>

<section id="<?php echo $section_id; ?>" class="content-contact small-gutters">
  <div class="row">
    <div class="col-12 col-sm-7 content">
      <?php the_content(); ?>
    </div>
    <?php if (has_post_thumbnail()): ?>
			<div class="col-12 col-sm-auto ml-auto image-wrapper">
	      <?php the_post_thumbnail('full'); ?>
	      <div class="overlay-empty"></div>
	    </div>
    <?php endif; ?>
  </div>

  <?php if ($address): ?>

    <div class="row">
      <address class="col-12 col-sm-7 address">
        <?php echo $address; ?>

        <?php if ($address_link): ?>
          <a href="<?php echo $address_link['url']; ?>" target="<?php echo $address_link['target']; ?>"><?php echo $address_link['title']; ?></a>
        <?php endif; ?>
      </address>
    </div>

  <?php endif; ?>
</section>

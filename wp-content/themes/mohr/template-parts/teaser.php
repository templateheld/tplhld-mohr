<?php
/**
 * Template part for controlling teaser
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>


<section id="teasers">
  <?php
    foreach ($teasers as $teaser) {
      $teaser_id = $teaser['teaser_link'];
      $teaser_template = get_field('teaser_template', $teaser_id);
      $teaser_template = str_replace(' ', '-', strtolower($teaser_template));
      $teaser = get_field($teaser_template, $teaser_id);
      #var_dump($teaser_id);
      #var_dump($teaser_template);
      #var_dump('template-parts/' . 'teaser-' . $teaser_template . '.php');
  ?>
    <article class="teaser template-<?php echo $teaser_template; ?><?php echo ($teaser_template == 'rechtsgebiete' ? ' small-gutters' : '') ?>">

      <?php if (locate_template('template-parts/' . 'teaser-' . $teaser_template . '.php')): ?>
          <?php include(locate_template('template-parts/' . 'teaser-' . $teaser_template . '.php')); ?>
      <?php endif; ?>

    </article>

  <?php } ?>

</section>

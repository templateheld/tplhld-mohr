<?php
/**
 * Template part for displaying teaser based on "DigiTube" template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>


<?php
  $digitube_link = $teaser['digitube_ubersichtsseite'];
?>



<div class="row">
  <div class="col-12 content">
    <h2 class="h1">Digi<br><span>Tube</span></h2>
    <a href="<?php echo $digitube_link['url']; ?>" target="<?php echo $digitube_link['target']; ?>"><?php echo $digitube_link['title']; ?></a>
  </div>
</div>

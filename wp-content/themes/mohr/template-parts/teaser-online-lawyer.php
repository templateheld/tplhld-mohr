<?php
/**
 * Template part for displaying teaser based on "Praxiskauf" template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  $teaser_image = $teaser['hintergrundbild'];
  $teaser_image_src = wp_get_attachment_image_src( $teaser_image, 'full' )[0];
  $teaser_image_srcset = wp_get_attachment_image_srcset( $teaser_image, 'full' );
  $teaser_image_sizes = wp_get_attachment_image_sizes( $teaser_image, 'full' );
  $teaser_image_alt = get_post_meta( $teaser_image, '_wp_attachment_image_alt', true);
  $teaser_title = $teaser['teaser_uberschrift'];
  $teaser_text = $teaser['teaser_text'];
  $teaser_link = $teaser['teaser_link'];
  $teaser_color = $teaser['overlay_farbe'];
?>

<div class="row">
  <div class="col-12 image-wrapper">
    <img src="<?php echo esc_attr( $teaser_image_src );?>"
    srcset="<?php echo esc_attr( $teaser_image_srcset ); ?>"
    sizes="<?php echo esc_attr( $teaser_image_sizes );?>"
    alt="<?php echo esc_attr( $teaser_image_alt );?>">
    <a href="<?php echo $teaser_link['url']; ?>" target="<?php echo $teaser_link['target']; ?>" class="col-9 col-sm-5 overlay<?php echo ($teaser_color == 'Gelb' ? ' overlay-yellow' : ''); ?>">
      <h2 class="h1"><?php echo $teaser_title; ?></h2>
      <div class="col-sm-7 p-sm-0 overlay-content">
        <?php echo $teaser_text; ?>
      </div>
      <span><?php echo $teaser_link['title']; ?></span>
    </a>
  </div>
</div>

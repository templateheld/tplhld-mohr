<?php
/**
 * Template part for displaying section "Zahlungsmöglichkeiten"
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  // block template
  if ($section_name == 'payments-block') {

?>
  <div class="row">
    <div class="col-12 col-sm-10 payments-block">
      <?php echo $section['inhalt'] ?>
      <div class="payments small-gutters">
        <div class="row">
          <div class="col-auto payment">
            <img class="paypal" src="<?php echo get_template_directory_uri() . '/assets/img/paypal.svg' ?>" alt="PayPal">
          </div>
          <div class="col-auto payment">
            <img class="visa" src="<?php echo get_template_directory_uri() . '/assets/img/visa.svg' ?>" alt="Visa">
          </div>
          <div class="col-auto payment">
            <img class="mastercard" src="<?php echo get_template_directory_uri() . '/assets/img/mastercard.svg' ?>" alt="Mastercard">
          </div>
          <div class="col-auto payment">
            <img class="sepa" src="<?php echo get_template_directory_uri() . '/assets/img/sepa.svg' ?>" alt="Sepa">
          </div>
          <div class="col-auto payment">
            <?php if(pll_current_language() == 'en'): ?>
            <img class="rechnung" src="<?php echo get_template_directory_uri() . '/assets/img/purchase.png' ?>" alt="Purchase on invoice">
            <?php else: ?>
            <img class="rechnung" src="<?php echo get_template_directory_uri() . '/assets/img/rechnung.png' ?>" alt="Kauf auf Rechnung">
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php } ?>

<?php
/**
 * Template part for displaying teaser based on "Video" template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */
?>

<?php
//replace related videos and loop the video if ends
$video_id = false;
if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $teaser, $match)) {
    $video_id = $match[1];
}
$teaser = str_replace('?feature=oembed','?feature=oembed&rel=0&loop=1&playlist='.$video_id,$teaser);
?>
<?php echo $teaser ?>

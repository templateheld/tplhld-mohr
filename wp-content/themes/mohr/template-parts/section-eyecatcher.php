<?php
/**
 * Template part for displaying section "Eyecatcher"
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  // Eyecatcher template
  if ($section_name == 'eyecatcher') {
    $eyecatcher_image = $section['eyecatcher_bild'];
    $eyecatcher_image_src = wp_get_attachment_image_src( $eyecatcher_image, 'full' )[0];
    $eyecatcher_image_srcset = wp_get_attachment_image_srcset( $eyecatcher_image, 'full' );
    $eyecatcher_image_sizes = wp_get_attachment_image_sizes( $eyecatcher_image, 'full' );
    $eyecatcher_image_alt = get_post_meta( $eyecatcher_image, '_wp_attachment_image_alt', true);
    $eyecatcher_title = str_replace(["\r\n", "\r", "\n"], '<strong>', $section['eyecatcher_uberschrift']) . '</strong>';
    $eyecatcher_caption = $section['eyecatcher_caption'];
?>
  <div class="row">
    <div id="eyecatcher-image">
      <img src="<?php echo esc_attr( $eyecatcher_image_src );?>"
      srcset="<?php echo esc_attr( $eyecatcher_image_srcset ); ?>"
      sizes="<?php echo esc_attr( $eyecatcher_image_sizes );?>"
      alt="<?php echo esc_attr( $eyecatcher_image_alt );?>">
      <header class="overlay">
        <a data-scroll class="overlay-link" href="#awards"></a>
        <h1><?php echo $eyecatcher_title ?></h1>
      </header>
    </div>
    <div id="eyecatcher-content" class="col">
      <h2><?php echo $eyecatcher_caption['inhalt_unteruberschrift']; ?></h2>
      <h2 class="headline"><?php echo $eyecatcher_caption['inhalt_uberschrift']; ?></h2>
      <?php echo $eyecatcher_caption['inhalt']; ?>
    </div>
  </div>

<?php } ?>

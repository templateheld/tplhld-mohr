<?php
/**
 * Template part for displaying section "Rechtsanwälte"
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  // Rechtsanwälte template
  if ($section_name == 'rechtsanwalte') {
    // Get all pages with "Rechtsanwalt" page template
    $lawyers = array(
      'post_type' => 'page',
      'posts_per_page' => -1,
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'meta_query' => array(
        array(
          'key' => '_wp_page_template',
          'value' => array('page-lawyer-highlighted.php')
        )
      )
    );
    $lawyers = new WP_Query($lawyers);
    $lawyers = $lawyers->posts;
?>

  <div class="row">

    <?php
      foreach ($lawyers as $lawyer):
        $lawyer_home_visibility = get_field('home_visible', $lawyer->ID);
        $lawyer_name = get_field('name', $lawyer->ID);
        $lawyer_position = get_field('position', $lawyer->ID);
        $lawyer_teaser_image = get_field('teaser_bild', $lawyer->ID);
        if (!$lawyer_teaser_image) {
          $lawyer_teaser_image = get_post_thumbnail_id($lawyer->ID);
        }
        $lawyer_teaser_image_src = wp_get_attachment_image_src( $lawyer_teaser_image, 'full' )[0];
        $lawyer_teaser_image_srcset = wp_get_attachment_image_srcset( $lawyer_teaser_image, 'full' );
        $lawyer_teaser_image_sizes = wp_get_attachment_image_sizes( $lawyer_teaser_image, 'full' );
        $lawyer_teaser_image_alt = get_post_meta( $lawyer_teaser_image, '_wp_attachment_image_alt', true);
        $lawyer_link = get_permalink($lawyer->ID);
        $lawyer_parent = wp_get_post_parent_id($lawyer->ID);
    ?>
    <?php
          #check if lawyer is hidden for the home page
          if( $lawyer_home_visibility == NULL ):
     ?>
        <article class="col-12 col-sm-4 lawyer">
          <img src="<?php echo esc_attr( $lawyer_teaser_image_src );?>"
          srcset="<?php echo esc_attr( $lawyer_teaser_image_srcset ); ?>"
          sizes="<?php echo esc_attr( $lawyer_teaser_image_sizes );?>"
          alt="<?php echo esc_attr( $lawyer_teaser_image_alt );?>">
          <a class="col-12 overlay" href="<?php echo $lawyer_link; ?>">
            <div class="lawyer-teaser">
              <h2><?php echo $lawyer_name; ?></h2>
              <h3 class="p"><?php echo $lawyer_position; ?></h3>
              <span><?php echo pll__('Read more') ?></span>
            </div>
          </a>
        </article>
    <?php endif; ?>

    <?php endforeach; ?>

    <?php
      $lawyer_parent_link = get_permalink($lawyer_parent);
      $lawyer_parent_name = get_the_title($lawyer_parent);
     ?>

     <aside>
       <a href="<?php echo $lawyer_parent_link; ?>"><?php echo $lawyer_parent_name; ?></a>
     </aside>

  </div>

<?php } ?>

<?php
/**
 * Template part for displaying page content in page-lawyer.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php $section_id = strtolower(clean(get_the_title())); ?>

<?php
  $name = get_field('name');
  $position = get_field('position');
  $image = get_post_thumbnail_id();
  $image_src = wp_get_attachment_image_src( $image, 'full' )[0];
  $image_srcset = wp_get_attachment_image_srcset( $image, 'full' );
  $image_sizes = wp_get_attachment_image_sizes( $image, 'full' );
  $image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
  $contact_data = get_field('kontaktdaten');
  $experience = get_field('erfahrung');
  $vita = get_field('vita');
  $memberships = get_field('mitgliedschaften');
  $talks = get_field('vortrage');
  $publications = get_field('publikationen');
?>

<section id="<?php echo $section_id; ?>" class="content-lawyer">
  <div class="row">
    <header class="col-12 content">
      <h1><?php echo $name; ?></h1>
      <p><?php echo $position; ?></p>
    </header>
  </div>
  <div class="row">
		<div class="col-12 col-sm-6 order-sm-2 lawyer-image-wrapper">
      <div class="row no-gutters">
        <div class="col-12  col-sm-auto ml-sm-auto image-wrapper">
          <img src="<?php echo esc_attr( $image_src );?>"
          srcset="<?php echo esc_attr( $image_srcset ); ?>"
          sizes="<?php echo esc_attr( $image_sizes );?>"
          alt="<?php echo esc_attr( $image_alt );?>">
          <div class="overlay-empty"></div>
        </div>
        <div class="col-12 d-sm-none lawyer-text contact">
          <p>
            <span class="phone"><?php echo pll__('Telephone'); ?><br> <a href="tel:<?php echo clean($contact_data['telefonnummer'], true); ?>"><?php echo $contact_data['telefonnummer']; ?></a></span>
            <br>
            <span class="e-mail">E-Mail <a href="mailto:<?php echo $contact_data['e-mailadresse']; ?>"><?php echo str_replace('@','@<br class="visible-xs">',$contact_data['e-mailadresse']); ?></a></span>
            <br>
            <a class="v-card" href="<?php echo $contact_data['v-card'] ?>" target="_blank">V-Card</a>
          </p>
        </div>
        <div class="col-12 lawyer-text experience">
          <?php if (!empty($experience['erfahrung_text'])): ?>
            <h2><?php echo pll__('Experience'); ?></h2>
            <?php echo strongToColor($experience['erfahrung_text']); ?>

            <?php if (!empty($experience['erfahrung_weitere_texte'])): ?>
              <div class="collapse" id="experience-more">
                <?php echo strongToColor($experience['erfahrung_weitere_texte']); ?>
              </div>
              <a class="read-more" data-toggle="collapse" href="#experience-more" aria-expanded="false" aria-controls="experience-more"><?php echo pll__('Learn more'); ?></a>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 order-sm-1  lawyer-text-wrapper">
      <div class="row d-none d-sm-block">
        <div class="col-12 lawyer-text contact">
          <p>
            <?php if (!empty($contact_data['telefonnummer'])): ?>
              <span class="phone"><?php echo pll__('Telephone'); ?><br> <a href="tel:<?php echo clean($contact_data['telefonnummer'], true); ?>"><?php echo $contact_data['telefonnummer']; ?></a></span>
              <br>
            <?php endif; ?>
            <?php if (!empty($contact_data['e-mailadresse'])): ?>
            <span class="e-mail">E-Mail <a href="mailto:<?php echo $contact_data['e-mailadresse']; ?>"><?php echo str_replace('@','@<br class="visible-xs">',$contact_data['e-mailadresse']); ?></a></span>
            <br>
            <?php endif; ?>
            <?php if (!empty($contact_data['v-card'])): ?>
              <a class="v-card" href="<?php echo $contact_data['v-card']; ?>" target="_blank">V-Card</a>
            <?php endif; ?>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 lawyer-text">
          <?php if (!empty($vita)): ?>
            <h2><?php echo pll__('Vita'); ?></h2>
            <?php echo strongToColor($vita); ?>
          <?php endif; ?>
        </div>

        <div class="col-12 lawyer-text">
          <?php if (!empty($memberships)): ?>
            <h2><?php echo pll__('Memberships'); ?></h2>
            <?php echo strongToColor($memberships); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-sm-6 lawyer-text-wrapper">
      <div class="row">
        <div class="col-12 lawyer-text">
          <?php if (!empty($talks)): ?>
            <h2><?php echo pll__('Talks'); ?></h2>
            <?php echo strongToColor($talks); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <div class="col-12 col-sm-6 lawyer-image-wrapper">
      <div class="row no-gutters">
        <div class="col-12 lawyer-text">
          <?php if (!empty($publications)): ?>
            <h2><?php echo pll__('Publications'); ?></h2>
            <?php echo strongToColor($publications); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

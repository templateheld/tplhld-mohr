<?php
/**
 * Template part for displaying section "Strategie Block"
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  // block template
  if ($section_name == 'strategie-block') {
    $cards = $section['cards'];
?>

  <div class="row">

    <?php
      foreach ($cards as $card) :
        $card_image = $card['card_bild'];
        $card_image_src = wp_get_attachment_image_src( $card_image, 'full' )[0];
        $card_image_srcset = wp_get_attachment_image_srcset( $card_image, 'full' );
        $card_image_sizes = wp_get_attachment_image_sizes( $card_image, 'full' );
        $card_image_alt = get_post_meta( $card_image, '_wp_attachment_image_alt', true);
        $card_title = $card['card_uberschrift'];
        $card_text = $card['card_inhalt'];
    ?>

    <article class="col-12 card">
      <div class="row">
        <div class="col-12 col-sm content">
          <h2><?php echo $card_title; ?></h2>
          <?php echo $card_text; ?>
        </div>
        <?php if ($card_image): ?>
          <div class="col-12 col-sm card-image">
            <img src="<?php echo esc_attr( $card_image_src );?>"
            srcset="<?php echo esc_attr( $card_image_srcset ); ?>"
            sizes="<?php echo esc_attr( $card_image_sizes );?>"
            alt="<?php echo esc_attr( $card_image_alt );?>">
          </div>
        <?php endif; ?>
      </div>
    </article>

    <?php endforeach; ?>

  </div>

<?php } ?>

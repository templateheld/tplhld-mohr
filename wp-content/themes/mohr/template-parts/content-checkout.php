<?php
/**
 * Template part for displaying page content in page-checkout.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>
<style>
    .woocommerce-order-received .form-mandatory { display: none; }
</style>
<section id="checkout" class="content-checkout">
  <div class="row header-shop">
    <header class="col-12 content">
      <div class="row no-gutters m-0">
        <div class="col-12 col-sm-auto">
          <h2 class="h1">Online<br><span>Lawyer</span></h2>
        </div>
        <div class="col-12 col-sm shop-slogan">
          <h3 class="h2"><?php echo pll__( 'Individual advice on a fixed price') ?></h3>
        </div>
      </div>
    </header>
  </div>
  <div class="row">
    <div class="col-12 content">
        <?php the_content(); ?>
        <div class="form-mandatory">
        <?php if(pll_current_language() == 'en'): ?>
            <p><sup style="color: red; font-size: 1.5rem">*</sup> The marked points are mandatory.</p>
        <?php else: ?>
            <p><sup style="color: red; font-size: 1.5rem">*</sup> Die gekennzeichneten Punkte sind Pflichtfelder.</p>
        <?php endif; ?>
        </div>
    </div>
    <?php if (has_post_thumbnail()): ?>
			<div class="col-12 image-wrapper">
	      <?php the_post_thumbnail('full'); ?>
	      <div class="overlay-empty"></div>
	    </div>
    <?php endif; ?>
  </div>
</section>

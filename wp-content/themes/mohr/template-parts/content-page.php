<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php $section_id = strtolower(clean(get_the_title())); ?>

<section id="<?php echo $section_id; ?>" class="content-page">
  <div class="row">
    <div class="col-12 col-sm-7 content">
      <?php the_content(); ?>
    </div>
    <?php
    //<div class="col-12 col-sm-3 content backto">
    //    <p>
    //        <a href=" echo wp_get_referer(); "><span> echo pll__( 'Back to overview' );</span></a>
    //</p>
    //</div>
    ?>
    <?php if (has_post_thumbnail()): ?>
			<div class="col-12 image-wrapper">
	      <?php the_post_thumbnail('full'); ?>
	      <div class="overlay-empty"></div>
	    </div>
    <?php endif; ?>

  </div>
</section>

<?php
$parent_title = get_the_title($post->post_parent);
if( $parent_title == 'Rechtsgebiete' || $parent_title == 'Areas of Law' ): ?>
    <div class="content-page">
        <div class="row">
            <div class="col-12 category-navigation posts-navigation">
                <div class="nav-links">
                    <div class="nav-next">
                        <a href="<?php echo wp_get_referer() ?>"><?php echo pll__( 'Back to overview' ); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
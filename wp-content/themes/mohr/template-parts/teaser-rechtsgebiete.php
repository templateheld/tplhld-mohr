<?php
/**
 * Template part for displaying teaser based on "Rechtsgebiete" template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  $legal_fields = $teaser['rechtsgebiete_link'];
  $legal_fields_index = $teaser['rechtsgebiete_ubersichtsseite'];
?>

<div class="row">
  <div class="col-12 col-sm-8 mx-auto content">
    <div class="link-cloud">

      <?php
        foreach ($legal_fields as $legal_field):
          $legal_field = $legal_field['rechtsgebiet'];
       ?>

       <span><a href="<?php echo $legal_field['url']; ?>" target="<?php echo $legal_field['target']; ?>"><?php echo $legal_field['title']; ?></a> /</span>

      <?php endforeach; ?>

    </div>
    <a href="<?php echo $legal_fields_index['url']; ?>" target="<?php echo $legal_fields_index['target']; ?>"><?php echo $legal_fields_index['title']; ?></a>
  </div>
</div>

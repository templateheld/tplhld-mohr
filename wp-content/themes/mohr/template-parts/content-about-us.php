<?php
/**
 * Template part for displaying page content in page-about-us.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php $section_id = strtolower(clean(get_the_title())); ?>

<section id="<?php echo $section_id; ?>" class="content-page small-gutters">
  <div class="row">
    <div class="col-12 col-sm-7 content">
      <?php the_content(); ?>
    </div>
    <?php if (has_post_thumbnail()): ?>
			<div class="col-12 col-sm-auto ml-auto image-wrapper">
	      <?php the_post_thumbnail('full'); ?>
	      <div class="overlay-empty"></div>
	    </div>
    <?php endif; ?>
  </div>
</section>

<?php
  $sections = get_field('inhaltsblocke', get_the_ID());

  if ($sections) {
    foreach ($sections as $section) :
      $section_name = $section['acf_fc_layout'];
      $section_name = clean($section_name);
      $section_attr = 'id="' . $section_name . '"';

      if (strpos($section_name, '-block')) {
        $section_attr = 'class="' . $section_name . '"';
      }
?>

  <section <?php echo $section_attr ?>>

    <?php if (locate_template('template-parts/' . 'section-' . $section_name . '.php')): ?>

      <?php include(locate_template('template-parts/' . 'section-' . $section_name . '.php')); ?>

    <?php else: ?>

      <div class="row">

        <div class="col-12 col-sm-10 content">
          <?php echo $section['inhalt'] ?>
        </div>

      </div>

    <?php endif; ?>

  </section>

<?php
    endforeach; 
  }
?>
<?php
$parent_title = get_the_title($post->post_parent);
if( $parent_title == 'Rechtsgebiete' || $parent_title == 'Areas of Law' ): ?>
    <div class="content-page">
        <div class="row">
            <div class="col-12 category-navigation posts-navigation">
                <div class="nav-links">
                    <div class="nav-next">
                        <a href="<?php echo wp_get_referer() ?>"><?php echo pll__( 'Back to overview' ); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
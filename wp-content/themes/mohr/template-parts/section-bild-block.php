<?php
/**
 * Template part for displaying section "Bild Block"
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  // block template
  if ($section_name == 'bild-block') {
    $block_image = $section['bild'];
    $block_image_src = wp_get_attachment_image_src( $block_image, 'full' )[0];
    $block_image_srcset = wp_get_attachment_image_srcset( $block_image, 'full' );
    $block_image_sizes = wp_get_attachment_image_sizes( $block_image, 'full' );
    $block_image_alt = get_post_meta( $block_image, '_wp_attachment_image_alt', true);
?>
  <div class="row">
    <div class="col-12 image-wrapper">
      <img src="<?php echo esc_attr( $block_image_src );?>"
      srcset="<?php echo esc_attr( $block_image_srcset ); ?>"
      sizes="<?php echo esc_attr( $block_image_sizes );?>"
      alt="<?php echo esc_attr( $block_image_alt );?>">
      <div class="overlay-empty"></div>
    </div>
  </div>

<?php } ?>

<?php
/**
 * Helper class which manages texts and plugin and shop versions.
 *
 * @package GiroCheckout
 */
define('__GIROCHECKOUT_TRANTYP_AUTH', 'AUTH');
define('__GIROCHECKOUT_TRANTYP_SALE', 'SALE');

class GiroCheckout_Utility {

  /**
   * Returns plugin version.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @return string
   */
  public static function getVersion() {
    return '4.0.2';
  }

  /**
   * Returns plugin and shop version.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @return string
   */
  public static function getGcSource() {
    return "WooCommerce " . WOOCOMMERCE_VERSION . ";WooCommerce Plugin " . self::getVersion();
  }
  
  

  /**
   * Get the payment purpose.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2014, GiroSolution AG
   * @return string
   */
  public static function getPurpose($p_strPurpose,$p_oOrder) {
    if( empty($p_strPurpose) ) {
      $strPurpose = "Best. {ORDERID}, {CUSTOMERNAME}";
    }
    else {
      $strPurpose = $p_strPurpose;
    }
    $strName = "";
    $strLastName = "";
    $strFirstName = "";  
    $strShopName = get_bloginfo('name');

    // For registered user
    if( is_user_logged_in() ) {
      $user_info = get_userdata( get_current_user_id() );
      $strName = $user_info->user_login;
      $strFirstName = $user_info->first_name;
      $strLastName = $user_info->last_name;
    }
    else {
      // For visitor
      $strFirstName = $p_oOrder->get_billing_first_name();
      $strLastName = $p_oOrder->get_billing_last_name();
      $strName = $strFirstName . " " .$strLastName;
    }

    if( method_exists($p_oOrder, 'get_id')) {
      $iOrderId = $p_oOrder->get_id();
    }
    else {
      $iOrderId = $p_oOrder->id;
    }

    $strPurpose = str_replace( "{ORDERID}", $iOrderId, $strPurpose );
    $strPurpose = str_replace( "{CUSTOMERID}", get_current_user_id(), $strPurpose );
    $strPurpose = str_replace( "{SHOPNAME}", $strShopName, $strPurpose );
    $strPurpose = str_replace( "{CUSTOMERNAME}", $strName, $strPurpose );
    $strPurpose = str_replace( "{CUSTOMERFIRSTNAME}", $strFirstName, $strPurpose );
    $strPurpose = str_replace( "{CUSTOMERLASTNAME}", $strLastName, $strPurpose );
    $strPurposeCod = mb_convert_encoding( $strPurpose, "UTF-8" );

    //return substr(utf8_encode($strPurposeCod), 0, 27);
    return substr($strPurposeCod, 0, 27);
  }  

  public static function formatText($p_strText) {
    return mb_convert_encoding( $p_strText, "UTF-8" );
  }    
}
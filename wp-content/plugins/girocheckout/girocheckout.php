<?php
/**
 * GiroCheckout
 *
 * @author      GiroSolution GmbH
 * @copyright   2017 Girosolution GmbH
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: GiroCheckout
 * Description: Plugin to integrate the GiroCheckout payment methods into WooCommerce.
 * Version:     4.0.2
 * Author:      GiroSolution GmbH
 * Author URI:  http://www.girosolution.de
 * Text Domain: girocheckout
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

include_once(realpath(dirname(__FILE__)) . '/library/GiroCheckout_SDK.php');
include_once(realpath(dirname(__FILE__)) . '/library/GiroCheckout_Utility.php');

add_action('plugins_loaded', 'woocommerce_girocheckout_init', 0);

function woocommerce_girocheckout_init() {
    if (!class_exists('WC_Payment_Gateway'))
        return;

    /**
     * Localisation
     */
    load_plugin_textdomain('girocheckout', false, dirname(plugin_basename(__FILE__)) . '/languages');

    /**
     * Gateway class
     */
    foreach (glob(dirname(__FILE__) . '/payments/*.php') as $filename) {
        include_once $filename;
    }

    /**
     * Add the Gateway to WooCommerce
     * */
    function woocommerce_add_girocheckout_gateway($methods) {

        $methods[] = 'gc_giropay';
        $methods[] = 'gc_directdebit';
        $methods[] = 'gc_creditcard';
        $methods[] = 'gc_ideal';
        $methods[] = 'gc_eps';
        $methods[] = 'gc_paydirekt';
        $methods[] = 'gc_sofortuw';
        $methods[] = 'gc_maestro';

        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'woocommerce_add_girocheckout_gateway');
}

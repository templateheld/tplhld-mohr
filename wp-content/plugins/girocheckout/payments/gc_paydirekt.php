<?php

class gc_paydirekt extends WC_Payment_Gateway
{

  /** @var string */
  public $merchantid;

  /** @var string */
  public $projectid;

  /** @var string */
  public $password;

  /** @var string */
  public $sourceid = 'e70f2793e318713d8491a2bbb98edfd7';

  /** @var string */
  public $lang;

  /** @var string */
  public $purpose;

  /** @var string */
  public $transactiontype;

  /** @var string */
  public $statuscapture;

  /**
   * Constructor for the gateway.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function __construct() {
    global $woocommerce;

    // Set language
    $this->setLanguage();

    // Set core gateway settings
    $this->id = 'gc_paydirekt';
    $this->method_title = __('Paydirekt', 'girocheckout');
    $this->title = __('Paydirekt', 'girocheckout');
    $this->icon = plugins_url('img/gc_paydirekt.jpg', dirname(__FILE__));
    $this->has_fields = FALSE;

    // Load the settings
    $this->init_form_fields();
    $this->init_settings();

    // Define user set variables
    $this->title = $this->get_option('title');
    $this->description = __('The online payment method of the German banks and Sparkassen. You will be redirected to the Paydirekt web site, where you will log into your personal account and authorize the payment. You require prior registration with Paydirekt. This payment method only works with German banking accounts.', 'girocheckout');
    $this->merchantid = $this->get_option('merchantid');
    $this->projectid = $this->get_option('projectid');
    $this->password = $this->get_option('password');
    $this->purpose = $this->get_option('purpose');
    $this->transactiontype = $this->get_option('transactiontype');
    $this->statuscapture = $this->get_option('statuscapture');

    // Hooks
    add_action('woocommerce_api_' . $this->id, array($this, 'check_response'));
    add_action('valid_redirect_' . $this->id, array($this, 'do_gc_redirect'));
    add_action('valid_notify_' . $this->id, array($this, 'do_gc_notify'));
    add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
    add_filter('woocommerce_payment_gateways', array($this, 'addGateway'));
    add_action('woocommerce_order_status_completed', array( $this, 'capture_payment' ) );
    add_action('woocommerce_order_status_processing', array( $this, 'capture_payment' ) );

    $this->supports = array(
      'products',
      'refunds'
    );
  }

  /**
   * Set language code for get text functions.
   *
   * Allowed values: de, en
   * default: de
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @param string $language
   * @return none
   */
  public function setLanguage($language = 'de') {
    $strLang = substr(get_bloginfo("language"), 0, 2);

    if ($strLang == 'de' || $strLang == 'en') {
      $this->lang = $strLang;
    }
    else {
      $this->lang = $language;
    }
  }

  /**
   * Initialise gateway settings form fields.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function init_form_fields() {
    $this->form_fields = array(
      'configuration' => array(
        'title' => __('Set-up configuration', 'girocheckout'),
        'type' => 'title'
      ),
      'enabled' => array(
        'title' => __('Enable/Disable', 'girocheckout'),
        'type' => 'checkbox',
        'label' => __('Enable Paydirekt', 'girocheckout'),
        'default' => 'no',
      ),
      'title' => array(
        'title' => __('Title', 'girocheckout'),
        'type' => 'text',
        'description' => __('Payment method title that the customer will see on your website.', 'girocheckout'),
        'default' => __('Paydirekt', 'girocheckout'),
        'desc_tip' => true,
      ),
      'merchantid' => array(
        'title' => __('Merchant ID', 'girocheckout'),
        'type' => 'text',
        'description' => __('Merchant ID from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'projectid' => array(
        'title' => __('Project ID', 'girocheckout'),
        'type' => 'text',
        'description' => __('Project ID from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'password' => array(
        'title' => __('Project password', 'girocheckout'),
        'type' => 'text',
        'description' => __('Project password from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'purpose' => array(
        'title' => __('Purpose', 'girocheckout'),
        'type' => 'text',
        'description' => __("You can define your own purpose using these placeholders:\n".
                              "{ORDERID}: Bestellnummer\n".
                              "{CUSTOMERID}: Kundennummer\n".
                              "{SHOPNAME}: Shop Name\n".
                              "{CUSTOMERNAME}: Kundenname\n".
                              "{CUSTOMERFIRSTNAME}: Kunde Vorname\n".
                              "{CUSTOMERLASTNAME}: Kunde Nachname\n".
                              "For example: If your purpose is \"Best. {ORDERID}, {SHOPNAME}\" then the submitted purpose must be \"Best. 55342, TestShop\"\n".
                              "The maximum length of the purpose is 27 characters.", 'girocheckout'),
        'default' => 'Best. {ORDERID}, {SHOPNAME}',
        'desc_tip' => true,
      ),
      'transactiontype' => array(
        'title'       => __( 'Transaction type', 'girocheckout' ),
        'type'        => 'select',
        'class'       => 'wc-enhanced-select',
        'default'     => 'authorize_sale',
        'options'     => array(
          'authorize'      => __( 'Authorize only', 'girocheckout' ),
          'authorize_sale' => __( 'Authorize and Sale', 'girocheckout' ),
        )
      ),
      'statuscapture' => array(
        'title'       => __( 'Automatically book the reserved amount on status change to:', 'girocheckout' ),
        'type'        => 'select',
        'class'       => 'wc-enhanced-select',
        'default'     => 'completed',
        'options'     => array(
          'completed'  => _x( 'Completed', 'Order status', 'girocheckout' ),
        )
      )
    );
  }

  /**
   * Add the payment gateway to wc.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return array
   */
  public function addGateway($methods) {

    if (get_woocommerce_currency() == "EUR") {
      $methods[] = $this->id;
    }

    return $methods;
  }

  /**
   * Admin Panel Options.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function admin_options() {
    ?>
    <h3><?php _e('Paydirekt', 'girocheckout'); ?></h3>
    <p><?php _e('GiroCheckout Paydirekt payment', 'girocheckout'); ?></p>
    <table class="form-table">
      <?php
      // Generate the HTML for the settings form.
      $this->generate_settings_html();
      ?>
    </table><!--/.form-table-->
    <?php
  }

  /**
   * Process the payment and return the result.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @param int $order_id
   * @return array
   */
  public function process_payment($order_id) {

    if (empty($this->merchantid)) {
      wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->projectid)) {
      wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->password)) {
      wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
      return;
    }

    $merchantId = $this->get_option('merchantid');
    $projectId = $this->get_option('projectid');
    $password = $this->get_option('password');
    $transactiontype = $this->get_option('transactiontype');

    try {
      $order = new WC_Order($order_id);

      if( method_exists($order, 'get_id')) {
        $orderID = $order->get_id();
      }
      else {
        $orderID = $order->id;
      }
      $amount = $order->get_total();
      $currency = get_woocommerce_currency();
      $transaction_id = $orderID;
      $urlRedirect = add_query_arg('type', 'redirect', add_query_arg('wc-api', $this->id, home_url('/')));
      $urlNotify = add_query_arg('type', 'notify', add_query_arg('wc-api', $this->id, home_url('/')));
      $strCartType = 'PHYSICAL';
      $iNumProdVirtual = 0;
      $iNumProdPhysical = 0;

      if( $transactiontype == "authorize" )
        $strTransType = __GIROCHECKOUT_TRANTYP_AUTH;
      else
        $strTransType = __GIROCHECKOUT_TRANTYP_SALE;

      $oCart = new GiroCheckout_SDK_Request_Cart();
      $aProducts = $order->get_items();

      if( method_exists($order, 'get_shipping_total')) {
        $iShippingWithoutTax = $order->get_shipping_total();
      }
      else {
        $iShippingWithoutTax = $order->order_shipping;
      }

      $iShippingTotal = $iShippingWithoutTax + $order->get_shipping_tax();
      $orderAmountExcShipping = $order->calculate_totals() - $iShippingWithoutTax;

      if (!empty($aProducts)) {
        foreach ($aProducts as $aProduct) {
          $item_id = $aProduct['product_id'];
          $product = new WC_Product($item_id);

          $iPrice = $product->get_price();
          /*if( $aProduct['total_tax'] > 0 ) {
            $iPrice = $product->get_price() + ($aProduct['total_tax']/$aProduct['qty']);
          }
          else {
            $iPrice = $product->get_price();
          }*/
          $oCart->addItem($aProduct['name'], $aProduct['qty'], $iPrice * 100, $product->get_sku());

          if( $product->is_downloadable() || $product->is_virtual() ) {
              $iNumProdVirtual++;
          }
          else{
              $iNumProdPhysical++;
          }
        }
      }

      // Set the shopping cart type
      if( $iNumProdVirtual > 0 && $iNumProdPhysical == 0) {
          $strCartType = 'DIGITAL';
      }
      elseif( $iNumProdVirtual > 0 && $iNumProdPhysical > 0) {
          $strCartType = 'MIXED';
      }

      if( method_exists($order, 'get_billing_first_name') ) {
        $strFirstName = $order->get_billing_first_name();
      }
      else {
        $strFirstName = $order->billing_first_name;
      }

      if( method_exists($order, 'get_billing_last_name') ) {
        $strLastName = $order->get_billing_last_name();
      }
      else {
        $strLastName = $order->billing_last_name;
      }

      if( method_exists($order, 'get_billing_company') ) {
        $strCompany = $order->get_billing_company();
      }
      else {
        $strCompany = $order->billing_company;
      }

      if( method_exists($order, 'get_billing_address_2') ) {
        $strAddress2 = $order->get_billing_address_2();
      }
      else {
        $strAddress2 = $order->billing_address_2;
      }

      if( method_exists($order, 'get_billing_address_1') ) {
        $strAddress1 = $order->get_billing_address_1();
      }
      else {
        $strAddress1 = $order->billing_address_1;
      }

      if( method_exists($order, 'get_billing_postcode') ) {
        $strPostCode = $order->get_billing_postcode();
      }
      else {
        $strPostCode = $order->billing_postcode;
      }

      if( method_exists($order, 'get_billing_city') ) {
        $strCity = $order->get_billing_city();
      }
      else {
        $strCity = $order->billing_city;
      }

      if( method_exists($order, 'get_billing_country') ) {
        $strCountry = $order->get_billing_country();
      }
      else {
        $strCountry = $order->billing_country;
      }

      if( method_exists($order, 'get_billing_email') ) {
        $strEmail = $order->get_billing_email();
      }
      else {
        $strEmail = $order->billing_email;
      }

      $request = new GiroCheckout_SDK_Request('paydirektTransaction');
      $request->setSecret($password);
      $request->addParam('merchantId', $merchantId)
                ->addParam('projectId', $projectId)
                ->addParam('merchantTxId', (int)$transaction_id)
                ->addParam('amount', round($amount * 100))
                ->addParam('currency', $currency)
                ->addParam('purpose', GiroCheckout_Utility::getPurpose($this->purpose, $order))
                ->addParam('shippingAmount', round($iShippingTotal * 100))
                ->addParam('shippingAddresseFirstName', GiroCheckout_Utility::formatText($strFirstName))
                ->addParam('shippingAddresseLastName', $strLastName)
                ->addParam('shippingCompany', $strCompany)
                ->addParam('shippingAdditionalAddressInformation', $strAddress2)
                ->addParam('shippingStreet', $strAddress1)
                ->addParam('shippingZipCode', $strPostCode)
                ->addParam('shippingCity', $strCity)
                ->addParam('shippingCountry', $strCountry)
                ->addParam('orderAmount', round($orderAmountExcShipping * 100))
                ->addParam('customerMail', $strEmail)
                ->addParam('cart', $oCart)
                ->addParam('urlRedirect', $urlRedirect)
                ->addParam('urlNotify', $urlNotify)
                ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
                ->addParam('orderId', (int)$transaction_id)
                ->addParam('customerId', get_current_user_id())
                ->addParam('shoppingCartType', $strCartType)
                ->addParam('shippingEmail', $strEmail)
                ->addParam('type', $strTransType)
                ->submit();

      if ($request->requestHasSucceeded($request)) {
        // Add the girocheckout transaction Id value to the order
        if (!add_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'), true)) {
          update_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'));
        }
        $strUrlRedirect = $request->getResponseParam('redirect');
      }
      else {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($request->getResponseParam('rc'), $this->lang), 'error');
        return;
      }
    }
    catch (Exception $e) {
      wc_add_notice(__('The plugin configuration data is false', 'girocheckout'), 'error');
      return;
    }

    return array(
      'result' => 'success',
      'redirect' => $strUrlRedirect,
    );
  }

  /**
   * Check the API response
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return none
   */
  public function check_response() {
    @ob_clean();

    if (!empty($_GET) && $_GET["type"] == 'redirect') {
      do_action("valid_redirect_" . $this->id);
    }
    else {
      do_action("valid_notify_" . $this->id);
    }
  }

  /**
   * Place to forward the customer back to the shop after the payment transaction.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function do_gc_redirect() {
    global $woocommerce;

    $password = $this->get_option('password');

    try {
      $notify = new GiroCheckout_SDK_Notify('paydirektTransaction');
      $notify->setSecret(trim((string)$password));
      $notify->parseNotification($_GET);

      $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
      $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
      $bPaymentSuccess = $notify->paymentSuccessful();

      if ($order->status != 'completed' && $order->status != 'processing') {
        // Checks if the payment was successful and redirects the user
        $order->add_order_note($paymentMsg);

        if ($bPaymentSuccess) {
          $order->payment_complete();
          // Remove cart
          $woocommerce->cart->empty_cart();
        }
        else {
          wc_add_notice($paymentMsg, 'error');
          $order->update_status('failed');
        }
      }
      else {
        if (!$bPaymentSuccess) {
          wc_add_notice($paymentMsg, 'error');
        }
        // Remove cart
        $woocommerce->cart->empty_cart();
      }
    }
    catch (Exception $e) {
      $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
      $order->add_order_note($e->getMessage());
      $order->update_status('failed');
    }

    wp_redirect($this->get_return_url($order));
  }

  /**
   * Place to notify to the shop of payment of this Web Giropay transfer.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function do_gc_notify() {
    global $woocommerce;

    $password = $this->get_option('password');

    try {
      $notify = new GiroCheckout_SDK_Notify('paydirektTransaction');
      $notify->setSecret(trim((string)$password));
      $notify->parseNotification($_GET);

      $iOrderId = $notify->getResponseParam('gcMerchantTxId');
      $order = new WC_Order($iOrderId);
      $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
      $order->add_order_note($paymentMsg);

      // Order already processed?
      if ($order->status == 'processing' || $order->status == 'completed') {
        $notify->sendOkStatus();
        $notify->setNotifyResponseParam('Result', 'ERROR');
        $notify->setNotifyResponseParam('ErrorMessage', "Order $iOrderId already had state=".$order->status);
        $notify->setNotifyResponseParam('MailSent', '');
        $notify->setNotifyResponseParam('OrderId', $iOrderId);
        $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
        echo $notify->getNotifyResponseStringJson();
        exit;
      }

      // Checks if the payment was successful
      if ($notify->paymentSuccessful()) {
        $order->payment_complete();
        // Remove cart
        $woocommerce->cart->empty_cart();

        $notify->sendOkStatus();
        $notify->setNotifyResponseParam('Result', 'OK');
        $notify->setNotifyResponseParam('ErrorMessage', '');
        $notify->setNotifyResponseParam('MailSent', '');
        $notify->setNotifyResponseParam('OrderId', $iOrderId);
        $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
        echo $notify->getNotifyResponseStringJson();
        exit;
      }
      else {
        $order->update_status('failed');
        exit;
      }
    }
    catch (Exception $e) {
      $iOrderId = $notify->getResponseParam('gcMerchantTxId');
      $order = new WC_Order($iOrderId);
      $notify->sendBadRequestStatus();
      $notify->setNotifyResponseParam('Result', 'ERROR');
      $notify->setNotifyResponseParam('ErrorMessage', $e->getMessage());
      $notify->setNotifyResponseParam('MailSent', '');
      $notify->setNotifyResponseParam('OrderId', $iOrderId);
      $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
      echo $notify->getNotifyResponseStringJson();
      exit;
    }
  }

  /**
   * Refund a charge
   * @param  int $order_id
   * @param  float $amount
   * @param  string $reason
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2014, GiroSolution AG
   * @return  boolean True or false based on success, or a WP_Error object
   */

  public function process_refund($order_id, $amount = null, $reason = '')
  {
    $order = new WC_Order($order_id);

    if (empty($this->merchantid)) {
      wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->projectid)) {
      wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->password)) {
      wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
      return;
    }

    $merchantId = $this->get_option('merchantid');
    $projectId = $this->get_option('projectid');
    $password = $this->get_option('password');
    $currency = get_woocommerce_currency();

    // Do your refund here. Refund $amount for the order with ID $order_id
    try {
      $strRef = get_post_meta($order_id, '_girocheckout_reference', true);
      $strRefCapture = get_post_meta($order_id, '_girocheckout_reference_capture', true);

      if( isset($strRefCapture) && strlen($strRefCapture) > 0 )
        $strReference = $strRefCapture;
      else
        $strReference = $strRef;

      $request = new GiroCheckout_SDK_Request('paydirektRefund');
      $request->setSecret($password);
      $request->addParam('merchantId', $merchantId)
        ->addParam('projectId', $projectId)
        ->addParam('merchantTxId', $order_id)
        ->addParam('amount', round($amount * 100))
        ->addParam('currency', $currency)
        ->addParam('reference', $strReference)
        ->addParam('purpose', $reason)
        ->submit();

      /* if the transaction did not succeed update your local system, get the responsecode and notify the customer */
      if (!$request->requestHasSucceeded()) {
        return false;
      }
    } catch (Exception $e) {
      return false;
    }

    return true;
  }

  /**
   * Capture payment when the order is changed from on-hold to complete or processing
   *
   * @param  int $order_id
   */
  public function capture_payment( $order_id )
  {
    $order = wc_get_order($order_id);
    $transactiontype = $this->get_option('transactiontype');
    $statuscapture = $this->get_option('statuscapture');
    $currency = get_woocommerce_currency();
    $amount = $order->order_total;
    $strReference = get_post_meta($order_id, '_girocheckout_reference', true);

    if( !empty($strReference) && $order->payment_method == 'gc_paydirekt' && $transactiontype == 'authorize' && $statuscapture == $order->status ) {

      if (empty($this->merchantid)) {
        wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
        return;
      }
      if (empty($this->projectid)) {
        wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
        return;
      }
      if (empty($this->password)) {
        wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
        return;
      }

      $merchantId = $this->get_option('merchantid');
      $projectId = $this->get_option('projectid');
      $password = $this->get_option('password');

      try {
        $request = new GiroCheckout_SDK_Request('paydirektCapture');
        $request->setSecret($password);
        $request->addParam('merchantId', $merchantId)
          ->addParam('projectId', $projectId)
          ->addParam('merchantTxId', $order_id)
          ->addParam('amount', round($amount * 100))
          ->addParam('currency', $currency)
          ->addParam('reference', $strReference)
          ->addParam('purpose', GiroCheckout_Utility::getPurpose($this->purpose, $order))
          ->submit();

        /* if transaction succeeded update your local system and redirect the customer */
        if ($request->requestHasSucceeded()) {
          $order->add_order_note(sprintf(__('Payment of %1$s was captured - Ref. ID: %2$s', 'woocommerce'), $order_id, $strReference));
          // Store the capture payment reference
          // Add the girocheckout transaction Id value to the order
          if (!add_post_meta($order_id, '_girocheckout_reference_capture', $request->getResponseParam('reference'), true)) {
            update_post_meta($order_id, '_girocheckout_reference_capture', $request->getResponseParam('reference'));
          }
        } /* if the transaction did not succeed update your local system, get the response message and notify the customer */
        else {
          $order->add_order_note(sprintf(__('Payment could not captured: %s', 'woocommerce'), $request->getResponseMessage($request->getResponseParam('rc'), 'DE')));
        }
      } catch (Exception $e) {
      }
    }
  }
}

<?php

class gc_directdebit extends WC_Payment_Gateway
{

  /** @var string */
  public $merchantid;

  /** @var string */
  public $projectid;

  /** @var string */
  public $password;

  /** @var string */
  public $sourceid = 'e70f2793e318713d8491a2bbb98edfd7';

  /** @var string */
  public $lang;

  /** @var string */
  public $bankcodefield;

  /** @var string */
  public $bankaccountfield;

  /** @var string */
  public $ibanfield;

  /** @var string */
  public $rbDirectdebitfield;

  /** @var string */
  public $accountholderfield;

  /** @var bool */
  public $showbankbata;

  /** @var string */
  public $purpose;

  /** @var bool */
  public $useexternalformservice;

  /** @var string */
  public $transactiontype;

  /** @var string */
  public $statuscapture;

  /**
   * Constructor for the gateway.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function __construct()
  {
    global $woocommerce;

    // Set language
    $this->setLanguage();

    // Set core gateway settings
    $this->id = 'gc_directdebit';
    $this->method_title = __('direct debit', 'girocheckout');
    $this->icon = plugins_url('img/gc_directdebit.png', dirname(__FILE__));
    $this->title = __('direct debit', 'girocheckout');
    $this->has_fields = true;

    // Load the settings
    $this->init_form_fields();
    $this->init_settings();

    // Define user set variables
    $this->title = $this->get_option('title');
    $this->description = __('Hereby you authorize us to charge your current account with the invoiced amount by direct debit.', 'girocheckout');
    $this->merchantid = $this->get_option('merchantid');
    $this->projectid = $this->get_option('projectid');
    $this->password = $this->get_option('password');
    $this->bankcodefield = $this->id . 'BankCode';
    $this->bankaccountfield = $this->id . 'BankAccount';
    $this->ibanfield = $this->id . 'IBAN';
    $this->rbDirectdebitfield = $this->id . 'rbDirectdebit';
    $this->accountholderfield = $this->id . 'Accountholder';
    $this->showbankbata = 'yes' === $this->get_option('showbankbata', 'no');
    $this->purpose = $this->get_option('purpose');
    $this->useexternalformservice = 'yes' === $this->get_option('useexternalformservice', 'no');
    $this->transactiontype = $this->get_option('transactiontype');
    $this->statuscapture = $this->get_option('statuscapture');

    // Hooks
    add_action('woocommerce_api_' . $this->id, array($this, 'check_response'));
    add_action('valid_redirect_' . $this->id, array($this, 'do_gc_redirect'));
    add_action('valid_notify_' . $this->id, array($this, 'do_gc_notify'));
    add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
    add_filter('woocommerce_payment_gateways', array($this, 'addGateway'));
    add_action('woocommerce_order_status_completed', array( $this, 'capture_payment' ) );
    add_action('woocommerce_order_status_processing', array( $this, 'capture_payment' ) );

    $this->supports = array(
      'products',
      'refunds'
    );
  }

  /**
   * Set language code for get text functions.
   *
   * Allowed values: de, en
   * default: de
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @param string $language
   * @return none
   */
  public function setLanguage($language = 'de')
  {
    $strLang = substr(get_bloginfo("language"), 0, 2);

    if ($strLang == 'de' || $strLang == 'en')
      $this->lang = $strLang;
    else
      $this->lang = $language;
  }

  /**
   * Initialise gateway settings form fields.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function init_form_fields()
  {
    $this->form_fields = array(
      'configuration' => array(
        'title' => __('Set-up configuration', 'girocheckout'),
        'type' => 'title'
      ),
      'enabled' => array(
        'title' => __('Enable/Disable', 'girocheckout'),
        'type' => 'checkbox',
        'label' => __('Enable direct debit', 'girocheckout'),
        'default' => 'no',
      ),
      'title' => array(
        'title' => __('Title', 'girocheckout'),
        'type' => 'text',
        'description' => __('Payment method title that the customer will see on your website.', 'girocheckout'),
        'default' => __('direct debit', 'girocheckout'),
        'desc_tip' => true,
      ),
      'merchantid' => array(
        'title' => __('Merchant ID', 'girocheckout'),
        'type' => 'text',
        'description' => __('Merchant ID from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'projectid' => array(
        'title' => __('Project ID', 'girocheckout'),
        'type' => 'text',
        'description' => __('Project ID from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'password' => array(
        'title' => __('Project password', 'girocheckout'),
        'type' => 'text',
        'description' => __('Project password from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'purpose' => array(
        'title' => __('Purpose', 'girocheckout'),
        'type' => 'text',
        'description' => __("You can define your own purpose using these placeholders:\n".
                              "{ORDERID}: Bestellnummer\n".
                              "{CUSTOMERID}: Kundennummer\n".
                              "{SHOPNAME}: Shop Name\n".
                              "{CUSTOMERNAME}: Kundenname\n".
                              "{CUSTOMERFIRSTNAME}: Kunde Vorname\n".
                              "{CUSTOMERLASTNAME}: Kunde Nachname\n".
                              "For example: If your purpose is \"Best. {ORDERID}, {SHOPNAME}\" then the submitted purpose must be \"Best. 55342, TestShop\"\n".
                              "The maximum length of the purpose is 27 characters.", 'girocheckout'),
        'default' => 'Best. {ORDERID}, {SHOPNAME}',
        'desc_tip' => true,
      ),
      'showbankbata' => array(
        'title' => __('Show Bank Account/ Bank Code input field','girocheckout'),
        'type' => 'checkbox',
        'label' => "",
        'default' => 'no',
      ),
      'useexternalformservice' => array(
        'title' => __('Use external form service', 'girocheckout'),
        'label' => "",
        'type' => 'checkbox',
        'default' => 'no',
      ),
      'transactiontype' => array(
        'title'       => __( 'Transaction type', 'girocheckout' ),
        'type'        => 'select',
        'class'       => 'wc-enhanced-select',
        'default'     => 'authorize_sale',
        'options'     => array(
          'authorize'      => __( 'Authorize only', 'girocheckout' ),
          'authorize_sale' => __( 'Authorize and Sale', 'girocheckout' ),
        )
      ),
      'statuscapture' => array(
        'title'       => __( 'Automatically book the reserved amount on status change to:', 'girocheckout' ),
        'type'        => 'select',
        'class'       => 'wc-enhanced-select',
        'default'     => 'completed',
        'options'     => array(
          'completed'  => _x( 'Completed', 'Order status', 'girocheckout' ),
        )
      )
    );
  }

  /**
   * Add the payment gateway to wc
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return array
   */
  public function addGateway($methods)
  {
    if (get_woocommerce_currency() == "EUR") {
      $methods[] = $this->id;
    }

    return $methods;
  }

  /**
   * Admin Panel Options.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function admin_options()
  {
    ?>
      <h3><?php _e('direct debit', 'girocheckout'); ?></h3>
      <p><?php _e('GiroCheckout Direct Debit payment', 'girocheckout'); ?></p>
      <table class="form-table">
        <?php
        // Generate the HTML for the settings form.
        $this->generate_settings_html();
        ?>
      </table><!--/.form-table-->
    <?php
  }

  /**
   * Payment form on checkout page.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function payment_fields()
  {
    if (isset($_POST[$this->rbDirectdebitfield])) {
      $rbDirectdebit = $_POST[$this->rbDirectdebitfield];
    }

    if ((isset($rbDirectdebit) && $rbDirectdebit == "rbIbanDirectdebit") ||
      !isset($rbDirectdebit)
    ) {
      $strCheckIban = " checked=\"checked\" ";
      $strCheckBank = "";
      $strDisplayIban = "block";
      $strDisplayBank = "none";
    } else {
      $strCheckIban = "";
      $strCheckBank = " checked=\"checked\" ";
      $strDisplayIban = "none";
      $strDisplayBank = "block";
    }
    ?>
      <script>
          function showBankData() {
              document.getElementById('divIbanDirectdebit').style.display = 'none';
              document.getElementById('divBankDataDirectdebit').style.display = 'block';
          }

          function showIban() {
              document.getElementById('divIbanDirectdebit').style.display = 'block';
              document.getElementById('divBankDataDirectdebit').style.display = 'none';
          }
      </script>
    <?php if ($this->description) : ?>
      <p><?php echo $this->description; ?></p>
  <?php endif; ?>
    <?php if (!$this->useexternalformservice) { ?>
    <?php if ($this->showbankbata) { ?>
          <p class="form-row">
              <input type="radio" name="<?php echo $this->rbDirectdebitfield; ?>"
                     class="gc_rbDirectdebit" <?php echo $strCheckIban; ?> value="rbIbanDirectdebit"
                     onclick="showIban();"><?php _e('iban', 'girocheckout'); ?>
              &nbsp;&nbsp;&nbsp;
              <input type="radio" name="<?php echo $this->rbDirectdebitfield; ?>"
                     class="gc_rbDirectdebit" <?php echo $strCheckBank; ?> value="rbBankDataDirectdebit"
                     onclick="showBankData();"><?php _e('bank account / bank code', 'girocheckout'); ?>
          </p>
    <?php } ?>
      <p class="form-row">
          <label for="<?php echo $this->accountholderfield; ?>">
            <?php _e('account holder', 'girocheckout'); ?>
              <abbr class="required" title="required">*</abbr>
          </label>
          <input type="text" name="<?php echo $this->accountholderfield; ?>"
                 id="<?php echo $this->accountholderfield; ?>" value=""/>
      <p>


          <div class="gc_directdebit" id="divBankDataDirectdebit" style="display:<?php echo $strDisplayBank; ?>;">
      <p class="form-row">
          <label for="<?php echo $this->bankaccountfield; ?>">
            <?php _e('bank account', 'girocheckout'); ?>
              <abbr class="required" title="required">*</abbr>
          </label>
          <input type="text" name="<?php echo $this->bankaccountfield; ?>"
                 id="<?php echo $this->bankaccountfield; ?>"/>
      </p>

      <p class="form-row">
          <label for="<?php echo $this->bankcodefield; ?>">
            <?php _e('bank code', 'girocheckout'); ?>
              <abbr class="required" title="required">*</abbr>
          </label>
          <input type="text" name="<?php echo $this->bankcodefield; ?>" id="<?php echo $this->bankcodefield; ?>"/>
      </p>
      </div>
      <div class="gc_directdebit" id="divIbanDirectdebit" style="display:<?php echo $strDisplayIban; ?>;">
          <p class="form-row">
              <label for="<?php echo $this->ibanfield; ?>">
                <?php _e('iban', 'girocheckout'); ?>
                  <abbr class="required" title="required">*</abbr>
              </label>
              <input type="text" name="<?php echo $this->ibanfield; ?>" id="<?php echo $this->ibanfield; ?>"/>
          </p>
      </div>
      </p>
    <?php
  }
  }

  /**
   * Process the payment and return the result.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @param int $order_id
   * @return array
   */
  public function process_payment($order_id)
  {
    global $woocommerce;

    if (empty($this->merchantid)) {
      wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->projectid)) {
      wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->password)) {
      wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
      return;
    }

    $bankaccount = $_POST[$this->bankaccountfield];
    $bankcode = $_POST[$this->bankcodefield];
    $iban = $_POST[$this->ibanfield];
    $rbDirectdebit = $_POST[$this->rbDirectdebitfield];
    $accountHolder = $_POST[$this->accountholderfield];

    $merchantId = $this->get_option('merchantid');
    $projectId = $this->get_option('projectid');
    $password = $this->get_option('password');
    $urlRedirect = add_query_arg('type', 'redirect', add_query_arg('wc-api', $this->id, home_url('/')));
    $urlNotify = add_query_arg('type', 'notify', add_query_arg('wc-api', $this->id, home_url('/')));
    $transactiontype = $this->get_option('transactiontype');

    try {
      $order = new WC_Order($order_id);
      $orderID = $order->id;
      $amount = $order->order_total;
      $currency = get_woocommerce_currency();
      $transaction_id = $orderID;

      if( $transactiontype == "authorize" )
        $strTransType = __GIROCHECKOUT_TRANTYP_AUTH;
      else
        $strTransType = __GIROCHECKOUT_TRANTYP_SALE;

      if ($this->useexternalformservice) {
        $request = new GiroCheckout_SDK_Request('directDebitTransactionWithPaymentPage');
        $request->setSecret($password);
        $request->addParam('merchantId', $merchantId)
          ->addParam('projectId', $projectId)
          ->addParam('merchantTxId', (int)$transaction_id)
          ->addParam('amount', round($amount * 100))
          ->addParam('currency', $currency)
          ->addParam('purpose', GiroCheckout_Utility::getPurpose($this->purpose, $order))
          ->addParam('urlRedirect', $urlRedirect)
          ->addParam('urlNotify', $urlNotify)
          ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
          ->addParam('orderId', (int)$transaction_id)
          ->addParam('customerId', get_current_user_id())
          ->addParam('type', $strTransType)
          ->submit();

        if ($request->requestHasSucceeded()) {
          // Add the girocheckout transaction Id value to the order
          if (!add_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'), true)) {
            update_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'));
          }
          $strUrlRedirect = $request->getResponseParam('redirect');
          return array(
            'result' => 'success',
            'redirect' => $strUrlRedirect,
          );
        } else {
          wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($request->getResponseParam('rc'), $this->lang), 'error');
          return;
        }
      }
      else {
    if ($this->showbankbata) {
      if ($rbDirectdebit == "rbIbanDirectdebit" && empty($iban)) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage(5027, $this->lang), 'error');
        return;
      } else if ($rbDirectdebit == "rbBankDataDirectdebit" && empty($bankcode)) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage(5024, $this->lang), 'error');
        return;
      } else if ($rbDirectdebit == "rbBankDataDirectdebit" && empty($bankaccount)) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage(5025, $this->lang), 'error');
        return;
      } else if (empty($accountHolder)) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage(5085, $this->lang), 'error');
        return;
      }
    } else {
      if (empty($iban)) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage(5027, $this->lang), 'error');
        return;
      } else if (empty($accountHolder)) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage(5085, $this->lang), 'error');
        return;
      }
    }

    $request = new GiroCheckout_SDK_Request('directDebitTransaction');
    $request->setSecret($password);
    $request->addParam('merchantId', $merchantId)
      ->addParam('projectId', $projectId)
      ->addParam('merchantTxId', (int)$transaction_id)
      ->addParam('amount', round($amount * 100))
      ->addParam('currency', $currency)
      ->addParam('purpose', GiroCheckout_Utility::getPurpose($this->purpose, $order))
      ->addParam('accountHolder', substr($accountHolder, 0, 27))
      ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
      ->addParam('orderId', (int)$transaction_id)
      ->addParam('type', $strTransType)
      ->addParam('customerId', get_current_user_id());

    if ($this->showbankbata) {
      if ($rbDirectdebit == 'rbIbanDirectdebit') {
        $request->addParam('iban', $iban);
      } else {
        $request->addParam('bankcode', $bankcode)
          ->addParam('bankaccount', $bankaccount);
      }
    } else {
      $request->addParam('iban', $iban);
    }

    $request->submit();

    if ($request->requestHasSucceeded() && $request->paymentSuccessful()) {
      // Add the girocheckout transaction Id value to the order
      if (!add_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'), true)) {
        update_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'));
      }
      $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($request->getResponseParam('rc'), $this->lang);
      $order->add_order_note($paymentMsg);

      if ($order->status != 'completed' && $order->status != 'processing') {
        $order->payment_complete();
        // Remove cart
        $woocommerce->cart->empty_cart();
      }
    } else {
      $order->update_status('failed');

      if (!$request->requestHasSucceeded()) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($request->getResponseParam('rc'), $this->lang), 'error');
        return;
      } else if (!$request->paymentSuccessful()) {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($request->getResponseParam('resultPayment'), $this->lang), 'error');
        return;
      }
    }
  }
    } catch (Exception $e)
{
wc_add_notice(__('The plugin configuration data is false','girocheckout'), 'error');
return;
}

return array(
  'result' => 'success',
  'redirect' => $this->get_return_url($order),
);
}

/**
 * Check the API response
 *
 * @author GiroSolution AG
 * @package GiroCheckout
 * @copyright Copyright (c) 2015, GiroSolution AG
 * @access public
 */
public
function check_response()
{
  @ob_clean();

  if (!empty($_GET) && $_GET["type"] == 'redirect') {
    do_action("valid_redirect_" . $this->id);
  } else {
    do_action("valid_notify_" . $this->id);
  }
}

/**
 * Place to forward the customer back to the shop after the payment transaction.
 *
 * @author GiroSolution AG
 * @package GiroCheckout
 * @copyright Copyright (c) 2015, GiroSolution AG
 * @access public
 */
public
function do_gc_redirect()
{
  global $woocommerce;

  $password = $this->get_option('password');

  try {
    $notify = new GiroCheckout_SDK_Notify('directDebitTransactionWithPaymentPage');
    $notify->setSecret(trim((string)$password));
    $notify->parseNotification($_GET);

    $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
    $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
    $bPaymentSuccess = $notify->paymentSuccessful();

    if ($order->status != 'processing' && $order->status != 'completed') {
      // Checks if the payment was successful and redirects the user
      $order->add_order_note($paymentMsg);

      if ($bPaymentSuccess) {
        $order->payment_complete();
        // Remove cart
        $woocommerce->cart->empty_cart();
      } else {
        wc_add_notice($paymentMsg, 'error');
        $order->update_status('failed');
      }
    } else {
      if (!$bPaymentSuccess) {
        wc_add_notice($paymentMsg, 'error');
      }
      // Remove cart
      $woocommerce->cart->empty_cart();
    }
  } catch (Exception $e) {
    $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
    $order->add_order_note($e->getMessage());
    $order->update_status('failed');
  }

  wp_redirect($this->get_return_url($order));
}

/**
 * Place to notify to the shop of payment of this Web Giropay transfer.
 *
 * @author GiroSolution AG
 * @package GiroCheckout
 * @copyright Copyright (c) 2015, GiroSolution AG
 * @access public
 */
public
function do_gc_notify()
{
  global $woocommerce;

  // Get the notification
  $password = $this->get_option('password');

  try {
    $notify = new GiroCheckout_SDK_Notify('directDebitTransactionWithPaymentPage');
    $notify->setSecret(trim((string)$password));
    $notify->parseNotification($_GET);

    $iOrderId = $notify->getResponseParam('gcMerchantTxId');
    $order = new WC_Order($iOrderId);
    $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
    $order->add_order_note($paymentMsg);

    // Order already processed?
    if ($order->status == 'processing' || $order->status == 'completed') {
      $notify->sendOkStatus();
      $notify->setNotifyResponseParam('Result', 'ERROR');
      $notify->setNotifyResponseParam('ErrorMessage', "Order $iOrderId already had state=" . $order->status);
      $notify->setNotifyResponseParam('MailSent', '');
      $notify->setNotifyResponseParam('OrderId', $iOrderId);
      $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
      echo $notify->getNotifyResponseStringJson();
      exit;
    }

    // Checks if the payment was successful
    if ($notify->paymentSuccessful()) {
      $order->payment_complete();
      // Remove cart
      $woocommerce->cart->empty_cart();

      $notify->sendOkStatus();
      $notify->setNotifyResponseParam('Result', 'OK');
      $notify->setNotifyResponseParam('ErrorMessage', '');
      $notify->setNotifyResponseParam('MailSent', '');
      $notify->setNotifyResponseParam('OrderId', $iOrderId);
      $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
      echo $notify->getNotifyResponseStringJson();
      exit;
    } else {
      $order->update_status('failed');
      exit;
    }
  } catch (Exception $e) {
    $iOrderId = $notify->getResponseParam('gcMerchantTxId');
    $order = new WC_Order($iOrderId);
    $notify->sendBadRequestStatus();
    $notify->setNotifyResponseParam('Result', 'ERROR');
    $notify->setNotifyResponseParam('ErrorMessage', $e->getMessage());
    $notify->setNotifyResponseParam('MailSent', '');
    $notify->setNotifyResponseParam('OrderId', $iOrderId);
    $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
    echo $notify->getNotifyResponseStringJson();
    exit;
  }
}

  /**
   * Refund a charge
   * @param  int $order_id
   * @param  float $amount
   * @param  string $reason
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2014, GiroSolution AG
   * @return  boolean True or false based on success, or a WP_Error object
   */

  public function process_refund($order_id, $amount = null, $reason = '')
  {
    $order = new WC_Order($order_id);

    if (empty($this->merchantid)) {
      wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->projectid)) {
      wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->password)) {
      wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
      return;
    }

    $merchantId = $this->get_option('merchantid');
    $projectId = $this->get_option('projectid');
    $password = $this->get_option('password');
    $currency = get_woocommerce_currency();

    // Do your refund here. Refund $amount for the order with ID $order_id
    try {
      $strRef = get_post_meta($order_id, '_girocheckout_reference', true);
      $strRefCapture = get_post_meta($order_id, '_girocheckout_reference_capture', true);

      if( isset($strRefCapture) && strlen($strRefCapture) > 0 )
        $strReference = $strRefCapture;
      else
        $strReference = $strRef;

      $request = new GiroCheckout_SDK_Request('directDebitRefund');
      $request->setSecret($password);
      $request->addParam('merchantId', $merchantId)
        ->addParam('projectId', $projectId)
        ->addParam('merchantTxId', $order_id)
        ->addParam('amount', round($amount * 100))
        ->addParam('currency', $currency)
        ->addParam('reference', $strReference)
        ->submit();

      /* if the transaction did not succeed update your local system, get the responsecode and notify the customer */
      if (!$request->requestHasSucceeded()) {
        return false;
      }
    } catch (Exception $e) {
      return false;
    }

    return true;
  }

  /**
   * Capture payment when the order is changed from on-hold to complete or processing
   *
   * @param  int $order_id
   */
  public function capture_payment( $order_id )
  {
    $order = wc_get_order($order_id);
    $transactiontype = $this->get_option('transactiontype');
    $statuscapture = $this->get_option('statuscapture');
    $strReference = get_post_meta($order_id, '_girocheckout_reference', true);

    if( !empty($strReference) && $order->payment_method == 'gc_directdebit' && $transactiontype == 'authorize' && $statuscapture == $order->status ) {
      if (empty($this->merchantid)) {
        wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
        return;
      }
      if (empty($this->projectid)) {
        wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
        return;
      }
      if (empty($this->password)) {
        wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
        return;
      }

      $merchantId = $this->get_option('merchantid');
      $projectId = $this->get_option('projectid');
      $password = $this->get_option('password');

      try {
        $currency = get_woocommerce_currency();
        $amount = $order->order_total;

        $request = new GiroCheckout_SDK_Request('directDebitCapture');
        $request->setSecret($password);
        $request->addParam('merchantId', $merchantId)
          ->addParam('projectId', $projectId)
          ->addParam('merchantTxId', $order_id)
          ->addParam('amount', round($amount * 100))
          ->addParam('currency', $currency)
          ->addParam('reference', $strReference)
          ->submit();

        /* if transaction succeeded update your local system and redirect the customer */
        if ($request->requestHasSucceeded()) {
          $order->add_order_note(sprintf(__('Payment of %1$s was captured - Ref. ID: %2$s', 'woocommerce'), $order_id, $strReference));
          // Store the capture payment reference
          // Add the girocheckout transaction Id value to the order
          if (!add_post_meta($order_id, '_girocheckout_reference_capture', $request->getResponseParam('reference'), true)) {
            update_post_meta($order_id, '_girocheckout_reference_capture', $request->getResponseParam('reference'));
          }
        } /* if the transaction did not succeed update your local system, get the response message and notify the customer */
        else {
          $order->add_order_note(sprintf(__('Payment could not captured: %s', 'woocommerce'), $request->getResponseMessage($request->getResponseParam('rc'), 'DE')));
        }
      } catch (Exception $e) {
      }
    }
  }
}
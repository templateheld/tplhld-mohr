<?php
class gc_eps extends WC_Payment_Gateway {

  /** @var string  */
  public $merchantid;

  /** @var string  */
  public $projectid;

  /** @var string  */
  public $password;

  /** @var string  */
  public $sourceid = 'e70f2793e318713d8491a2bbb98edfd7';

  /** @var string  */
  public $lang;

  /** @var string  */
  public $bankcodefield;

  /** @var string  */
  public $purpose;

  /** @var bool  */
  public $useexternalformservice;

  /**
   * Constructor for the gateway.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function __construct() {
    global $woocommerce;

    // Set language
    $this->setLanguage();

    // Set core gateway settings
    $this->id = 'gc_eps';
    $this->method_title = __('eps - online bank transfer (AT)', 'girocheckout');
    $this->icon = plugins_url('img/gc_eps.jpg', dirname(__FILE__));
    $this->title = __('eps - online bank transfer (AT)', 'girocheckout');
    $this->has_fields = true;

    // Load the settings
    $this->init_form_fields();
    $this->init_settings();

    // Define user set variables
    $this->title = $this->get_option('title');
    $this->description = __('With eps online transfer you pay simply, fast and secure by online banking of your bank. Your will be redirected to the online banking at your bank where you authorize the credit transfer with PIN and TAN.', 'girocheckout');
    $this->merchantid = $this->get_option('merchantid');
    $this->projectid = $this->get_option('projectid');
    $this->password = $this->get_option('password');
    $this->bankcodefield = $this->id . 'BankCode';
    $this->purpose = $this->get_option('purpose');
    $this->useexternalformservice = 'yes' === $this->get_option('useexternalformservice', 'no');

    // Hooks
    add_action('woocommerce_api_' . $this->id, array($this, 'check_response'));
    add_action('valid_redirect_' . $this->id, array($this, 'do_gc_redirect'));
    add_action('valid_notify_' . $this->id, array($this, 'do_gc_notify'));
    add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
    add_filter('woocommerce_payment_gateways', array($this, 'addGateway'));
    add_action('wp_enqueue_scripts', array($this, 'load_eps_widget'));
  }

  public function load_eps_widget() {
    wp_enqueue_script('girocheckout_script', 'https://bankauswahl.girocheckout.de/widget/v2/girocheckoutwidget.min.js', array(), '1.0.0', true);
    wp_register_style('girocheckout_style', 'https://bankauswahl.girocheckout.de/widget/v2/style.min.css');
    wp_enqueue_style('girocheckout_style');
  }

  /**
   * Set language code for get text functions.
   *
   * Allowed values: de, en
   * default: de
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @param string $language
   * @return none
   */
  public function setLanguage($language = 'de') {
    $strLang = substr(get_bloginfo("language"), 0, 2);

    if ($strLang == 'de' || $strLang == 'en')
      $this->lang = $strLang;
    else
      $this->lang = $language;
  }

  /**
   * Initialise gateway settings form fields.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function init_form_fields() {
    $this->form_fields = array(
        'configuration' => array(
            'title' => __('Set-up configuration', 'girocheckout'),
            'type' => 'title'
        ),
        'enabled' => array(
            'title' => __('Enable/Disable', 'girocheckout'),
            'type' => 'checkbox',
            'label' => __('Enable eps - online bank transfer (AT)', 'girocheckout'),
            'default' => 'no',
        ),
        'title' => array(
            'title' => __('Title', 'girocheckout'),
            'type' => 'text',
            'description' => __('Payment method title that the customer will see on your website.', 'girocheckout'),
            'default' => __('eps - online bank transfer (AT)', 'girocheckout'),
            'desc_tip' => true,
        ),
        'merchantid' => array(
            'title' => __('Merchant ID', 'girocheckout'),
            'type' => 'text',
            'description' => __('Merchant ID from GiroCockpit', 'girocheckout'),
            'default' => '',
            'desc_tip' => true,
        ),
        'projectid' => array(
            'title' => __('Project ID', 'girocheckout'),
            'type' => 'text',
            'description' => __('Project ID from GiroCockpit', 'girocheckout'),
            'default' => '',
            'desc_tip' => true,
        ),
        'password' => array(
            'title' => __('Project password', 'girocheckout'),
            'type' => 'text',
            'description' => __('Project password from GiroCockpit', 'girocheckout'),
            'default' => '',
            'desc_tip' => true,
        ),
        'purpose' => array(
            'title' => __('Purpose', 'girocheckout'),
            'type' => 'text',
            'description' => __("You can define your own purpose using these placeholders:\n".
                                  "{ORDERID}: Bestellnummer\n".
                                  "{CUSTOMERID}: Kundennummer\n".
                                  "{SHOPNAME}: Shop Name\n".
                                  "{CUSTOMERNAME}: Kundenname\n".
                                  "{CUSTOMERFIRSTNAME}: Kunde Vorname\n".
                                  "{CUSTOMERLASTNAME}: Kunde Nachname\n".
                                  "For example: If your purpose is \"Best. {ORDERID}, {SHOPNAME}\" then the submitted purpose must be \"Best. 55342, TestShop\"\n".
                                  "The maximum length of the purpose is 27 characters.", 'girocheckout'),
            'default' => 'Best. {ORDERID}, {SHOPNAME}',
            'desc_tip' => true,
        ),
        'useexternalformservice' => array(
            'title' => __('Use external form service', 'girocheckout'),
            'label' => "",
            'type' => 'checkbox',
            'default' => 'no',
        ),
    );
  }

  /**
   * Add the payment gateway to wc
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return array
   */
  public function addGateway($methods) {
    if (get_woocommerce_currency() == "EUR") {
      $methods[] = $this->id;
    }

    return $methods;
  }

  /**
   * Admin Panel Options.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function admin_options() {
    ?>
    <h3><?php _e('eps - online bank transfer (AT)', 'girocheckout'); ?></h3>
    <p><?php _e('GiroCheckout eps payment', 'girocheckout'); ?></p>
    <table class="form-table">
      <?php
      // Generate the HTML for the settings form.
      $this->generate_settings_html();
      ?>
    </table><!--/.form-table-->
    <?php
  }

  /**
   * Payment form on checkout page.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function payment_fields() {
    ?>
    <style type="text/css">
      .ui-eps_widget {
        position:absolute;
        z-index: 1000;
      }
    </style>      
    <?php if ($this->description) : ?>
      <p><?php echo $this->description; ?></p>
    <?php endif; ?>
    <?php if( !$this->useexternalformservice ) { ?>
    <p class="form-row">
      <label for="<?php echo $this->bankcodefield; ?>">
    <?php _e('bank', 'girocheckout'); ?>
        <abbr class="required" title="required">*</abbr>
      </label>
      <input type="text" name="<?php echo $this->bankcodefield; ?>" id="<?php echo $this->bankcodefield; ?>" value="" onkeyup="girocheckout_widget(this, event, 'bic', '3')"/><?php _e('(Search by bank name, bank code, city or bic.)', 'girocheckout'); ?>
    </p>
    <?php
    }
  }

  /**
   * Process the payment and return the result.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @param int $order_id
   * @return array
   */
  public function process_payment($order_id) {
    global $woocommerce;

    if (empty($this->merchantid)) {
      wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->projectid)) {
      wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->password)) {
      wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
      return;
    }

    $merchantId = $this->get_option('merchantid');
    $projectId = $this->get_option('projectid');
    $password = $this->get_option('password');

    try {
      $order = new WC_Order($order_id);
      $orderID = $order->id;
      $amount = $order->order_total;
      $currency = get_woocommerce_currency();
      $transaction_id = $orderID;
      $urlRedirect = add_query_arg('type', 'redirect', add_query_arg('wc-api', $this->id, home_url('/')));
      $urlNotify = add_query_arg('type', 'notify', add_query_arg('wc-api', $this->id, home_url('/')));

      if( $this->useexternalformservice ) {
        // Sends request to Girocheckout.
        $reqPayment = new GiroCheckout_SDK_Request('epsTransaction');
        $reqPayment->setSecret($password);
        $reqPayment->addParam('merchantId', $merchantId)
                   ->addParam('projectId', $projectId)
                   ->addParam('merchantTxId', (int) $transaction_id)
                   ->addParam('amount', round($amount * 100))
                   ->addParam('currency', $currency)
                   ->addParam('purpose', GiroCheckout_Utility::getPurpose($this->purpose, $order))
                   ->addParam('urlRedirect', $urlRedirect)
                   ->addParam('urlNotify', $urlNotify)
                   ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
                   ->addParam('orderId', (int) $transaction_id)
                   ->addParam('customerId', get_current_user_id())
                   ->submit();

        if ($reqPayment->requestHasSucceeded()) {
          $strUrlRedirect = $reqPayment->getResponseParam('redirect');
        } else {
          wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($reqPayment->getResponseParam('rc'), $this->lang), 'error');
          return;
        }
      }
      else {
        if (isset($_POST[$this->bankcodefield])) {
          $bankcode = $_POST[$this->bankcodefield];

          if( empty($bankcode) ) {
            wc_add_notice(__('Please specify your BIC if you want to pay with eps.', 'girocheckout'), 'error');
            return;
          }

          //Check if the bank provides eps from BIC.
          $request = new GiroCheckout_SDK_Request('epsBankstatus');
          $request->setSecret($password);
          $request->addParam('merchantId', $merchantId)
                  ->addParam('projectId', $projectId)
                  ->addParam('bic', $bankcode)
                  ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
                  ->submit();

          if ($request->requestHasSucceeded()) {

            // Sends request to Girocheckout.
            $reqPayment = new GiroCheckout_SDK_Request('epsTransaction');
            $reqPayment->setSecret($password);
            $reqPayment->addParam('merchantId', $merchantId)
                       ->addParam('projectId', $projectId)
                       ->addParam('merchantTxId', (int) $transaction_id)
                       ->addParam('amount', round($amount * 100))
                       ->addParam('currency', $currency)
                       ->addParam('purpose', GiroCheckout_Utility::getPurpose($this->purpose, $order))
                       ->addParam('bic', $bankcode)
                       ->addParam('urlRedirect', $urlRedirect)
                       ->addParam('urlNotify', $urlNotify)
                       ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
                       ->addParam('orderId', (int) $transaction_id)
                       ->addParam('customerId', get_current_user_id())
                       ->submit();

            if ($reqPayment->requestHasSucceeded()) {
              $strUrlRedirect = $reqPayment->getResponseParam('redirect');
            } else {
              wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($reqPayment->getResponseParam('rc'), $this->lang), 'error');
              return;
            }
          } else {
            wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($request->getResponseParam('rc'), $this->lang), 'error');
            return;
          }
        }
      }
    } catch (Exception $e) {
      wc_add_notice(__('The plugin configuration data is false','girocheckout'), 'error');
      return;
    }  

    return array(
        'result' => 'success',
        'redirect' => $strUrlRedirect,
    );
  }

  /**
   * Check the API response
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function check_response() {
    @ob_clean();

    if (!empty($_GET) && $_GET["type"] == 'redirect') {
      do_action("valid_redirect_" . $this->id);
    } else {
      do_action("valid_notify_" . $this->id);
    }
  }

  /**
   * Place to forward the customer back to the shop after the payment transaction.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function do_gc_redirect() {
    global $woocommerce;

    $password = $this->get_option('password');

    try {
      $notify = new GiroCheckout_SDK_Notify('epsTransaction');
      $notify->setSecret(trim((string) $password));
      $notify->parseNotification($_GET);

      $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
      $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
      $bPaymentSuccess = $notify->paymentSuccessful();
      
      if ($order->status != 'processing' && $order->status != 'completed') {
        // Checks if the payment was successful and redirects the user
        $order->add_order_note($paymentMsg);

        if( $bPaymentSuccess ) {
          $order->payment_complete();
          // Remove cart
          $woocommerce->cart->empty_cart();
        }
        else {
          wc_add_notice($paymentMsg, 'error');
          $order->update_status('failed');
        }
      }
      else {
        if( !$bPaymentSuccess ) {
          wc_add_notice($paymentMsg, 'error');
        }
        // Remove cart
        $woocommerce->cart->empty_cart();
      }
    }
    catch (Exception $e) {
      $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
      $order->add_order_note($e->getMessage());
      $order->update_status('failed');
    }

    wp_redirect($this->get_return_url($order));
  }

  /**
   * Place to notify to the shop of payment of this Web Giropay transfer.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function do_gc_notify() {
    global $woocommerce;

    // Get the notification
    $password = $this->get_option('password');

    try {
      $notify = new GiroCheckout_SDK_Notify('epsTransaction');
      $notify->setSecret(trim((string) $password));
      $notify->parseNotification($_GET);

      $iOrderId = $notify->getResponseParam('gcMerchantTxId');
      $order = new WC_Order($iOrderId);
      $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
      $order->add_order_note($paymentMsg);

      // Order already processed?
      if ($order->status == 'processing' || $order->status == 'completed') {
        $notify->sendOkStatus();
        $notify->setNotifyResponseParam('Result', 'ERROR');
        $notify->setNotifyResponseParam('ErrorMessage', "Order $iOrderId already had state=".$order->status);
        $notify->setNotifyResponseParam('MailSent', '');
        $notify->setNotifyResponseParam('OrderId', $iOrderId);
        $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
        echo $notify->getNotifyResponseStringJson();
        exit;
      }

      // Checks if the payment was successful 
      if ($notify->paymentSuccessful()) {
        $order->payment_complete();
        // Remove cart
        $woocommerce->cart->empty_cart();

        $notify->sendOkStatus();
        $notify->setNotifyResponseParam('Result', 'OK');
        $notify->setNotifyResponseParam('ErrorMessage', '');
        $notify->setNotifyResponseParam('MailSent', '');
        $notify->setNotifyResponseParam('OrderId', $iOrderId);
        $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
        echo $notify->getNotifyResponseStringJson();
        exit;
      }
      else {
        $order->update_status('failed');
        exit;
      }
    }
    catch (Exception $e) {
      $iOrderId = $notify->getResponseParam('gcMerchantTxId');
      $order = new WC_Order($iOrderId);
      $notify->sendBadRequestStatus();
      $notify->setNotifyResponseParam('Result', 'ERROR');
      $notify->setNotifyResponseParam('ErrorMessage', $e->getMessage());
      $notify->setNotifyResponseParam('MailSent', '');
      $notify->setNotifyResponseParam('OrderId', $iOrderId);
      $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
      echo $notify->getNotifyResponseStringJson();
      exit;
    }
  }
}

<?php

class gc_ideal extends WC_Payment_Gateway
{

  /** @var string */
  public $merchantid;

  /** @var string */
  public $projectid;

  /** @var string */
  public $password;

  /** @var string */
  public $sourceid = 'e70f2793e318713d8491a2bbb98edfd7';

  /** @var string */
  public $lang;

  /** @var string */
  public $bankcodefield;

  /** @var string */
  public $purpose;

  /**
   * Constructor for the gateway.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function __construct()
  {
    global $woocommerce;

    // Set language
    $this->setLanguage();

    // Set core gateway settings
    $this->id = 'gc_ideal';
    $this->method_title = __('iDEAL - online bank transfer (NL)', 'girocheckout');
    $this->icon = plugins_url('img/gc_ideal.jpg', dirname(__FILE__));
    $this->title = __('iDEAL - online bank transfer (NL)', 'girocheckout');
    $this->has_fields = true;

    // Load the settings
    $this->init_form_fields();
    $this->init_settings();

    // Define user set variables
    $this->title = $this->get_option('title');
    $this->description = __('With iDEAL you pay simply, fast and secure by online banking of your bank. You will be redirected to the online banking of your bank where you authorize the credit transfer with PIN and TAN.', 'girocheckout');
    $this->merchantid = $this->get_option('merchantid');
    $this->projectid = $this->get_option('projectid');
    $this->password = $this->get_option('password');
    $this->bankcodefield = $this->id . 'BankCode';
    $this->purpose = $this->get_option('purpose');

    // Hooks
    add_action('woocommerce_api_' . $this->id, array($this, 'check_response'));
    add_action('valid_redirect_' . $this->id, array($this, 'do_gc_redirect'));
    add_action('valid_notify_' . $this->id, array($this, 'do_gc_notify'));
    add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
    add_action('init', array($this, 'gcStartSession'), 1);
    add_action('wp_logout', array($this, 'gcEndSession'));
    add_action('wp_login', array($this, 'gcEndSession'));
    add_filter('woocommerce_payment_gateways', array($this, 'addGateway'));

    $this->supports = array(
      'products',
      'refunds'
    );
  }

  public function gcStartSession()
  {
    if (!session_id()) {
      session_start();
    }
  }

  public function gcEndSession()
  {
    session_destroy();
  }

  /**
   * Set language code for get text functions.
   *
   * Allowed values: de, en
   * default: de
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @param string $language
   */
  public function setLanguage($language = 'de')
  {
    $strLang = substr(get_bloginfo("language"), 0, 2);

    if ($strLang == 'de' || $strLang == 'en')
      $this->lang = $strLang;
    else
      $this->lang = $language;
  }

  /**
   * Initialise gateway settings form fields.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function init_form_fields()
  {
    $this->form_fields = array(
      'configuration' => array(
        'title' => __('Set-up configuration', 'girocheckout'),
        'type' => 'title'
      ),
      'enabled' => array(
        'title' => __('Enable/Disable', 'girocheckout'),
        'type' => 'checkbox',
        'label' => __('Enable iDEAL - online bank transfer (NL)', 'girocheckout'),
        'default' => 'no',
      ),
      'title' => array(
        'title' => __('Title', 'girocheckout'),
        'type' => 'text',
        'description' => __('Payment method title that the customer will see on your website.', 'girocheckout'),
        'default' => __('iDEAL - online bank transfer (NL)', 'girocheckout'),
        'desc_tip' => true,
      ),
      'merchantid' => array(
        'title' => __('Merchant ID', 'girocheckout'),
        'type' => 'text',
        'description' => __('Merchant ID from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'projectid' => array(
        'title' => __('Project ID', 'girocheckout'),
        'type' => 'text',
        'description' => __('Project ID from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'password' => array(
        'title' => __('Project password', 'girocheckout'),
        'type' => 'text',
        'description' => __('Project password from GiroCockpit', 'girocheckout'),
        'default' => '',
        'desc_tip' => true,
      ),
      'purpose' => array(
        'title' => __('Purpose', 'girocheckout'),
        'type' => 'text',
        'description' => __("You can define your own purpose using these placeholders:\n".
                              "{ORDERID}: Bestellnummer\n".
                              "{CUSTOMERID}: Kundennummer\n".
                              "{SHOPNAME}: Shop Name\n".
                              "{CUSTOMERNAME}: Kundenname\n".
                              "{CUSTOMERFIRSTNAME}: Kunde Vorname\n".
                              "{CUSTOMERLASTNAME}: Kunde Nachname\n".
                              "For example: If your purpose is \"Best. {ORDERID}, {SHOPNAME}\" then the submitted purpose must be \"Best. 55342, TestShop\"\n".
                              "The maximum length of the purpose is 27 characters.", 'girocheckout'),
        'default' => 'Best. {ORDERID}, {SHOPNAME}',
        'desc_tip' => true,
      ),
    );
  }

  /**
   * Add the payment gateway to wc
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return array
   */
  public function addGateway($methods)
  {
    if (get_woocommerce_currency() == "EUR") {
      $methods[] = $this->id;
    }

    return $methods;
  }

  /**
   * Admin Panel Options.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function admin_options()
  {
    ?>
      <h3><?php _e('iDEAL - online bank transfer (NL)', 'girocheckout'); ?></h3>
      <p><?php _e('GiroCheckout iDEAL payment', 'girocheckout'); ?></p>
      <table class="form-table">
        <?php
        // Generate the HTML for the settings form.
        $this->generate_settings_html();
        ?>
      </table><!--/.form-table-->
    <?php
  }

  /**
   *
   * Payment form on checkout page
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @return void
   */
  public function payment_fields()
  {
    ?>
    <?php if ($this->description) : ?>
      <p><?php echo $this->description; ?></p>
  <?php endif; ?>
      <select name="<?php echo $this->bankcodefield; ?>" id="<?php echo $this->bankcodefield; ?>"
              class="giroconnect-bankcode form-select">
        <?php
        $merchantId = $this->get_option('merchantid');
        $projectId = $this->get_option('projectid');
        $password = $this->get_option('password');

        /* if request succeeded get the issuer list */
        if (isset($_SESSION['aIssuerLst'])) {
          $issuers = $_SESSION['aIssuerLst'];
        } else {
          try {
            //Sends request to Girocheckout to get the list of the issuers.
            $request = new GiroCheckout_SDK_Request('idealIssuerList');
            $request->setSecret($password);
            $request->addParam('merchantId', $merchantId)
                    ->addParam('projectId', $projectId)
                    ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
                    ->submit();

            if ($request->requestHasSucceeded()) {
              $issuers = $request->getResponseParam('issuer');
              $_SESSION['aIssuerLst'] = $issuers;
            }
          } catch (Exception $e) {

          }
        }
        $choosen = '';
        $selected = ' selected="selected"';

        foreach ($issuers as $key => $label) {
          if ($choosen !== '') {
            echo '<option' . $selected . ' value="' . $key . '">' . $label . '</option>';
            $selected = '';
          } else {
            echo '<option' . (($key == $choosen) ? $selected : '') . ' value="' . $key . '">' . $label . '</option>';
          }
        }
        ?>
      </select>
    <?php
  }

  /**
   * Process the payment and return the result.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   * @param int $order_id
   * @return array
   */
  public function process_payment($order_id)
  {
    global $woocommerce;

    if (empty($this->merchantid)) {
      wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->projectid)) {
      wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->password)) {
      wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
      return;
    }

    $bankcode = $_POST[$this->bankcodefield];
    $merchantId = $this->get_option('merchantid');
    $projectId = $this->get_option('projectid');
    $password = $this->get_option('password');

    try {
      $order = new WC_Order($order_id);
      $orderID = $order->id;
      $amount = $order->order_total;
      $currency = get_woocommerce_currency();
      $transaction_id = $orderID;
      $urlRedirect = add_query_arg('type', 'redirect', add_query_arg('wc-api', $this->id, home_url('/')));
      $urlNotify = add_query_arg('type', 'notify', add_query_arg('wc-api', $this->id, home_url('/')));

      //Sends request to Girocheckout.
      $request = new GiroCheckout_SDK_Request('idealPayment');
      $request->setSecret($password);
      $request->addParam('merchantId', $merchantId)
        ->addParam('projectId', $projectId)
        ->addParam('merchantTxId', (int)$transaction_id)
        ->addParam('amount', round($amount * 100))
        ->addParam('currency', $currency)
        ->addParam('purpose', GiroCheckout_Utility::getPurpose($this->purpose, $order))
        ->addParam('issuer', $bankcode)
        ->addParam('urlRedirect', $urlRedirect)
        ->addParam('urlNotify', $urlNotify)
        ->addParam('sourceId', GiroCheckout_Utility::getGcSource())
        ->addParam('orderId', (int)$transaction_id)
        ->addParam('customerId', get_current_user_id())
        ->submit();

      if ($request->requestHasSucceeded()) {
        // Add the girocheckout transaction Id value to the order
        if (!add_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'), true)) {
          update_post_meta($orderID, '_girocheckout_reference', $request->getResponseParam('reference'));
        }
        $strUrlRedirect = $request->getResponseParam('redirect');
      } else {
        wc_add_notice(GiroCheckout_SDK_ResponseCode_helper::getMessage($request->getResponseParam('rc'), $this->lang), 'error');
        return;
      }
    } catch (Exception $e) {
      wc_add_notice(__('The plugin configuration data is false', 'girocheckout'), 'error');
      return;
    }

    return array(
      'result' => 'success',
      'redirect' => $strUrlRedirect,
    );
  }

  /**
   * Check the API response
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function check_response()
  {
    @ob_clean();

    if (!empty($_GET) && $_GET["type"] == 'redirect') {
      do_action("valid_redirect_" . $this->id);
    } else {
      do_action("valid_notify_" . $this->id);
    }
  }

  /**
   * Place to forward the customer back to the shop after the payment transaction.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function do_gc_redirect()
  {
    global $woocommerce;

    $password = $this->get_option('password');

    try {
      $notify = new GiroCheckout_SDK_Notify('idealPayment');
      $notify->setSecret(trim((string)$password));
      $notify->parseNotification($_GET);

      $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
      $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
      $bPaymentSuccess = $notify->paymentSuccessful();

      if ($order->status != 'completed' && $order->status != 'processing') {
        // Checks if the payment was successful and redirects the user
        $order->add_order_note($paymentMsg);

        if ($bPaymentSuccess) {
          $order->payment_complete();
          // Remove cart
          $woocommerce->cart->empty_cart();
        } else {
          wc_add_notice($paymentMsg, 'error');
          $order->update_status('failed');
        }
      } else {
        if (!$bPaymentSuccess) {
          wc_add_notice($paymentMsg, 'error');
        }
        // Remove cart
        $woocommerce->cart->empty_cart();
      }
    } catch (Exception $e) {
      $order = new WC_Order($notify->getResponseParam('gcMerchantTxId'));
      $order->add_order_note($e->getMessage());
      $order->update_status('failed');
    }

    wp_redirect($this->get_return_url($order));
  }

  /**
   * Place to notify to the shop of payment of this Web Giropay transfer.
   *
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2015, GiroSolution AG
   * @access public
   */
  public function do_gc_notify()
  {
    global $woocommerce, $_SESSION;

    $password = $this->get_option('password');

    try {
      $notify = new GiroCheckout_SDK_Notify('idealPayment');
      $notify->setSecret(trim((string)$password));
      $notify->parseNotification($_GET);

      $iOrderId = $notify->getResponseParam('gcMerchantTxId');
      $order = new WC_Order($iOrderId);
      $paymentMsg = GiroCheckout_SDK_ResponseCode_helper::getMessage($notify->getResponseParam('gcResultPayment'), $this->lang);
      $order->add_order_note($paymentMsg);

      // Order already processed?
      if ($order->status == 'processing' || $order->status == 'completed') {
        $notify->sendOkStatus();
        $notify->setNotifyResponseParam('Result', 'ERROR');
        $notify->setNotifyResponseParam('ErrorMessage', "Order $iOrderId already had state=" . $order->status);
        $notify->setNotifyResponseParam('MailSent', '');
        $notify->setNotifyResponseParam('OrderId', $iOrderId);
        $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
        echo $notify->getNotifyResponseStringJson();
        exit;
      }

      // Checks if the payment was successful 
      if ($notify->paymentSuccessful()) {
        unset($_SESSION['aIssuerLst']);

        $order->payment_complete();
        // Remove cart
        $woocommerce->cart->empty_cart();

        $notify->sendOkStatus();
        $notify->setNotifyResponseParam('Result', 'OK');
        $notify->setNotifyResponseParam('ErrorMessage', '');
        $notify->setNotifyResponseParam('MailSent', '');
        $notify->setNotifyResponseParam('OrderId', $iOrderId);
        $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
        echo $notify->getNotifyResponseStringJson();
        exit;
      } else {
        $order->update_status('failed');
        exit;
      }
    } catch (Exception $e) {
      $iOrderId = $notify->getResponseParam('gcMerchantTxId');
      $order = new WC_Order($iOrderId);
      $notify->sendBadRequestStatus();
      $notify->setNotifyResponseParam('Result', 'ERROR');
      $notify->setNotifyResponseParam('ErrorMessage', $e->getMessage());
      $notify->setNotifyResponseParam('MailSent', '');
      $notify->setNotifyResponseParam('OrderId', $iOrderId);
      $notify->setNotifyResponseParam('CustomerId', $order->get_user_id());
      echo $notify->getNotifyResponseStringJson();
      exit;
    }
  }

  /**
   * Refund a charge
   * @param  int $order_id
   * @param  float $amount
   * @param  string $reason
   * @author GiroSolution AG
   * @package GiroCheckout
   * @copyright Copyright (c) 2014, GiroSolution AG
   * @return  boolean True or false based on success, or a WP_Error object
   */

  public function process_refund($order_id, $amount = null, $reason = '')
  {
    $order = new WC_Order($order_id);

    if (empty($this->merchantid)) {
      wc_add_notice(__('GiroCheckout error: missing Merchant-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->projectid)) {
      wc_add_notice(__('GiroCheckout error: missing Project-ID', 'girocheckout'), 'error');
      return;
    }
    if (empty($this->password)) {
      wc_add_notice(__('GiroCheckout error: missing project passphrase', 'girocheckout'), 'error');
      return;
    }

    $merchantId = $this->get_option('merchantid');
    $projectId = $this->get_option('projectid');
    $password = $this->get_option('password');
    $currency = get_woocommerce_currency();

    // Do your refund here. Refund $amount for the order with ID $order_id
    try {
      $strReference = get_post_meta($order_id, '_girocheckout_reference', true);
      $request = new GiroCheckout_SDK_Request('idealRefund');
      $request->setSecret($password);
      $request->addParam('merchantId', $merchantId)
        ->addParam('projectId', $projectId)
        ->addParam('merchantTxId', $order_id)
        ->addParam('amount', round($amount * 100))
        ->addParam('currency', $currency)
        ->addParam('reference', $strReference)
        ->submit();

      /* if the transaction did not succeed update your local system, get the responsecode and notify the customer */
      if (!$request->requestHasSucceeded()) {
        return false;
      }
    } catch (Exception $e) {
      return false;
    }

    return true;
  }
}
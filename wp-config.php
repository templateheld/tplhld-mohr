<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mohr' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T>y#]n;&+qLLJTJ(?njH4 mJx5 )<VSWfTN&sP6gHkvOhvo8U|*%%Awdd&SCv1mV' );
define( 'SECURE_AUTH_KEY',  'b1:m%M+pystOv6Ou2<GcCD&whk:J89]_xt,mVVm#!YHXf=Hs*Z]cH+mBay%?9dhL' );
define( 'LOGGED_IN_KEY',    '=6Sj&6;9:Fwa8@FAvAR@VMP[jNO5PM8phB?KjxNqq+JXU$`64lu`L=|ta%#LuQn2' );
define( 'NONCE_KEY',        '5fxbwg1<5wXOARj,xAeq-ILYk{KR/-n9Zt3D*=W2rG[-T5ZRuY W  N=##W[ )!f' );
define( 'AUTH_SALT',        'yR%Gv-[U?Q_b2i0=rJ]Y@7SG#%&G6)xP4Bp9GK*r}6juU`g{v}3Vjqa;LYMa=-Bc' );
define( 'SECURE_AUTH_SALT', 'FcEZ!8W9_a[*{d$b;)<tC<U{vgB5VjD/=0d^cPX>,U>~, M#83,@W0R|f%@Kx@V{' );
define( 'LOGGED_IN_SALT',   '%bijk93UF#M/E+apL9~nkw_Wyd<+c*<4M@2Mz39HD&zeSzDz=kvCj?RFjv%G%d`y' );
define( 'NONCE_SALT',       'M  sD:}NM4?H4~%xqW)qmue_BnP#MJRM6{DbXNLE]Y~w4&_USF}yw(dug8l21(Vt' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define( 'WC_GZD_DIRECT_DEBIT_KEY', 'def0000035741ab210ffe82ab346c91814433f60e75c1dda45fc701f4bc0ef25c0c4667f98c2ec1cea888062fbf7a73eea8246690f871fad3d630c0c79dbbcb55ddcbd66' );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
